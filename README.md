# D&D XP tracker for 5th Edition

## Build tooling
Several experimental compiler arguments are enabled in the project's `build.gradle`, because _cutting edge_.
- `-Xopt-in=kotlin.RequiresOptIn` - enables the use of several experimental classes, mainly around Jetpack Compose

### Dependencies
Dependencies are declared in the `Libs.kt` file, with their versions centralized in the `Versions.kt` file, both under the `buildSrc` module.

#### Checking for updates
This project uses the [version-checker-gradle-lint](https://github.com/PicPay/version-checker-gradle-lint) plugin. To install it, please run the `versionCheckerInstall.sh` script. Configuration variables for this Lint check are in the `buildSrc/versionlint.properties`.

When installed, this plugin will trigger warnings when dependencies are outdated, although it doesn't always work properly with cutting edge (`-alpha`/`-beta`) versions.

### Lint
This project uses Spotless + KtLint to enforce a default code style. Configurations are present in the project `build.gradle` and applied to any and all subprojects.

## CI
GitlabCI is configured to run debug builds and unit tests for every commit in every branch. For every commit into the `master` branch, it will also run a release build, sign it, and upload it to the internal test track in Play Store (using [fastlane](https://fastlane.tools/)). Therefore, the `master` branch is protected and every contribution must be made through a Merge Request.

All of the necessary keys are defined through environmental variables in Gitlab's UI, which are masked in job logs.
