package com.goncalojoaoc.xptracker

import androidx.compose.ui.test.*
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.test.espresso.Espresso
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.goncalojoaoc.xptracker.constants.ADD_CAMPAIGN
import com.goncalojoaoc.xptracker.constants.NAVIGATION_ICON
import com.goncalojoaoc.xptracker.data.BootstrapModule
import com.goncalojoaoc.xptracker.presentation.main.MainActivity
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@UninstallModules(BootstrapModule::class)
@RunWith(AndroidJUnit4::class)
class NavigationDrawerTest {

    @get:Rule
    val rule = createAndroidComposeRule<MainActivity>()

    @Test
    fun navigationDrawerOpenCloseTest() {
        rule.onNodeWithTag(NAVIGATION_ICON)
            .assertExists()
            .performClick()

        rule.onNodeWithText("5E XP Tracker").assertIsDisplayed()

        Espresso.pressBack()

        rule.onNodeWithText("5E XP Tracker").assertIsNotDisplayed()
    }

    @Test
    fun addCampaignTest(){
        rule.onNodeWithTag(NAVIGATION_ICON)
            .assertExists()
            .performClick()

        rule.onNodeWithTag(ADD_CAMPAIGN)
            .assertExists()
            .performClick()

        rule.onNodeWithText("Create a campaign").assertIsDisplayed()

        Espresso.pressBack()

        rule.onNodeWithTag(NAVIGATION_ICON)
            .assertExists()
            .performClick()

        rule.onNodeWithText("New Campaign").assertIsDisplayed()

    }

    @Test
    fun clickOnCampaign(){
        rule.onNodeWithTag(NAVIGATION_ICON)
            .assertExists()
            .performClick()

        rule.onNodeWithText("The Idiot Emporium").performClick()

        rule.onNodeWithText("Recent Sessions").assertIsDisplayed()
    }
}