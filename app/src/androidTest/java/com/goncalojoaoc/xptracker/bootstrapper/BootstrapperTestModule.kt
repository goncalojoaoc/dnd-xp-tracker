package com.goncalojoaoc.xptracker.bootstrapper

import com.goncalojoaoc.xptracker.data.BootstrapModule
import com.goncalojoaoc.xptracker.data.IBootstrapper
import dagger.Binds
import dagger.Module
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.testing.TestInstallIn

@Module
@TestInstallIn(components = [ApplicationComponent::class], replaces = [BootstrapModule::class])
abstract class BootstrapperTestModule {

    @Binds
    abstract fun bindTestBootstrapper(impl: TestBootstrapper): IBootstrapper
}