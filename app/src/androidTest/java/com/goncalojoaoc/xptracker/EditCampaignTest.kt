package com.goncalojoaoc.xptracker

import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.test.espresso.Espresso
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.goncalojoaoc.xptracker.constants.*
import com.goncalojoaoc.xptracker.data.BootstrapModule
import com.goncalojoaoc.xptracker.data.IBootstrapper
import com.goncalojoaoc.xptracker.presentation.main.MainActivity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@HiltAndroidTest
@UninstallModules(BootstrapModule::class)
@RunWith(AndroidJUnit4::class)
class EditCampaignTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    val rule = createAndroidComposeRule<MainActivity>()

    @Inject
    lateinit var bootstrapper: IBootstrapper

    @Before
    fun setup(){
        hiltRule.inject()
        bootstrapper.ensureDefaultData()
    }

    private fun editCampaign(){
        rule.onNodeWithTag(NAVIGATION_ICON)
            .assertExists()
            .performClick()

        rule.onNodeWithText("The Idiot Emporium").performClick()

        rule.onNodeWithText("Recent Sessions").assertIsDisplayed()

        rule.onNodeWithTag(DROPDOWN_MENU).performClick()

        rule.onNodeWithText("Edit campaign").performClick()
    }

    @Test
    @Ignore("Ignored due to jetpack compose bug")
    fun createNewCharacterAndCheckLevel(){
        editCampaign()

        rule.onNodeWithTag(ADD_CHARACTER).performClick()

        rule.onNodeWithTag(CHARACTER_NAME).performTextInput("Nera")

        rule.onNodeWithTag(CURRENT_XP).performTextReplacement("315")

        rule.onNodeWithText("Save").performClick()

        rule.onNodeWithText("Edit campaign").assertIsDisplayed()

        rule.onNodeWithText("Nera")
            .onChildren()
            .assertAny(hasText("315 XP"))
            .assertAny(hasText("Level 2"))
    }

    @Test
    fun deleteCharacterConfirm(){
        editCampaign()

        rule.onAllNodesWithTag(DELETE_CHARACTER).onFirst().performClick()

        rule.onNodeWithText("Are you sure you want to delete this character?").assertExists()

        rule.onNodeWithText("Yes").performClick()

        rule.onNodeWithText("The Idiot Emporium")

        rule.onNodeWithText("Lord Mc Edgelord II").assertDoesNotExist()
    }

    @Test
    fun deleteCharacterDismiss(){
        editCampaign()

        rule.onAllNodesWithTag(DELETE_CHARACTER).onFirst().performClick()

        rule.onNodeWithText("Are you sure you want to delete this character?").assertExists()

        rule.onNodeWithText("No").performClick()

        rule.onNodeWithText("The Idiot Emporium")

        rule.onNodeWithText("Lord Mc Edgelord II").assertIsDisplayed()
    }

    @Test
    @Ignore("Ignored due to jetpack compose bug")
    fun editCharacter(){
        editCampaign()

        rule.onAllNodesWithTag(EDIT_CHARACTER).onFirst().performClick()

        rule.onNodeWithTag(CHARACTER_NAME).performTextInput("Nera")

        rule.onNodeWithTag(CURRENT_XP).performTextInput("315")

        rule.onNodeWithText("Save").performClick()

        rule.onNodeWithText("Edit campaign").assertIsDisplayed()

        rule.onNodeWithText("Nera")
            .onChildren()
            .assertAny(hasText("315 XP"))
            .assertAny(hasText("Level 2"))
    }
}