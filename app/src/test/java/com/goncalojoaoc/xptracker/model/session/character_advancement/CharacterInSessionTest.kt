package com.goncalojoaoc.xptracker.model.session.character_advancement

import org.junit.Assert.*
import org.junit.Test

class CharacterInSessionTest {

    private val character = CharacterInSession(
        name = "Character",
        characterId = 1,
        campaignId = 1,
        sessionNumber = 1,
        advancement = CharacterAdvancement(),
        startingXp = 0
    )

    @Test
    fun `should calculate the XP earned in a session`() {
        val sut = character.copy(
            advancement = CharacterAdvancement(
                listOf(
                    Encounter(100),
                    EpicMoment(20),
                    Other(10, "something"),
                    Encounter(200)
                )
            )
        )

        val expected = 330

        val actual = sut.sessionXp

        assertEquals(expected, actual)
    }

    @Test
    fun `should calculate the total XP after a session`() {
        val sut = character.copy(
            startingXp = 100,
            advancement = CharacterAdvancement(Encounter(200))
        )

        val expected = 300

        val actual = sut.xpAfterSession

        assertEquals(expected, actual)
    }

}