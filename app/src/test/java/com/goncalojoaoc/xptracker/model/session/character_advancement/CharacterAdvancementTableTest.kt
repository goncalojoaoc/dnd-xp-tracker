package com.goncalojoaoc.xptracker.model.session.character_advancement

import com.goncalojoaoc.xptracker.model.character.Character
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancementTable.hasLeveledUp
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class CharacterAdvancementTableTest {

    private val sut = CharacterAdvancementTable

    @Test
    fun `XP per level table should be valid`() {
        var previousLevel = sut.startingLevel
        var previousXpValue = 0
        sut.xpLevelThresholds.onEachIndexed { index, entry ->
            if (index != 0 && index != sut.xpLevelThresholds.size - 1) {
                assertTrue(previousLevel < entry.key)
                assertTrue(previousXpValue < entry.value)
                previousLevel = entry.key
                previousXpValue = entry.value
            }
        }
        assertTrue(sut.xpLevelThresholds.keys.maxOf { it } <= sut.endLevel)
    }

    @Test
    fun `should calculate level between thresholds`() {
        val expected = 6
        val actual = sut.levelAtXp(15_000)

        assertEquals(expected, actual)
    }

    @Test
    fun `should calculate level at XP threshold`() {
        val expected = 14
        val actual = sut.levelAtXp(140_000)

        assertEquals(expected, actual)
    }

    @Test
    fun `should calculate first level XP`() {
        val expected = 1
        val actual = sut.levelAtXp(150)

        assertEquals(expected, actual)
    }

    @Test
    fun `should calculate epic moment XP`() {
        val xpAtSecondLevel = 300
        val modifier = 15
        val expected = EpicMoment(30)

        val actual = sut.calculateEpicMomentXp(modifier = modifier, characterXp = xpAtSecondLevel)

        assertEquals(expected.awardedXp, actual.awardedXp)
    }

    @Test
    fun `should set character XP after session`() {
        val charsInSession = listOf(
            CharacterInSession(
                name = "Character 1",
                campaignId = 1,
                characterId = 1,
                sessionNumber = 1,
                startingXp = 100,
                advancement = CharacterAdvancement(Encounter(100))
            ),
            CharacterInSession(
                name = "Character 2",
                campaignId = 1,
                characterId = 2,
                sessionNumber = 1,
                startingXp = 0,
                advancement = CharacterAdvancement(
                    listOf(
                        Encounter(100),
                        EpicMoment(15),
                        Other(15, "Extra Boost")
                    )
                )
            )
        )
        val characters = listOf(
            Character(
                name = "Character 1",
                id = 1,
                campaignId = 1,
                xp = 100
            ),
            Character(
                name = "Character 2",
                id = 2,
                campaignId = 1,
                xp = 0
            )
        )
        val expected = listOf(
            Character(
                name = "Character 1",
                id = 1,
                campaignId = 1,
                xp = 200
            ),
            Character(
                name = "Character 2",
                id = 2,
                campaignId = 1,
                xp = 130
            )
        )

        val actual = sut.advanceCharacters(characters, charsInSession)

        assertEquals(expected, actual)
    }

    @Test
    fun `should correctly identify leveled up characters`() {
        val character = CharacterInSession(
            name = "name",
            campaignId = 1,
            sessionNumber = 1,
            characterId = 1,
            startingXp = 100,
            advancement = CharacterAdvancement(Encounter(200))
        )

        assertTrue(character.hasLeveledUp)
    }
}
