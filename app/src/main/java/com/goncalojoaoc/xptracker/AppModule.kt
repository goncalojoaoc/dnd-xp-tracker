package com.goncalojoaoc.xptracker

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Named

@Module
@InstallIn(ApplicationComponent::class)
class AppModule {

    companion object {
        const val IS_DEBUG =" IS_DEBUG"
    }

    @Provides
    @Named(IS_DEBUG)
    fun provideIsDebug(): Boolean = BuildConfig.DEBUG
}
