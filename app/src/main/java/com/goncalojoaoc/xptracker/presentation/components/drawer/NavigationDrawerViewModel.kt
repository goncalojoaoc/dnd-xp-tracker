package com.goncalojoaoc.xptracker.presentation.components.drawer

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.goncalojoaoc.xptracker.data.ICampaignsRepository

class NavigationDrawerViewModel @ViewModelInject constructor(
    private val campaignsRepository: ICampaignsRepository
) : ViewModel() {

    val campaigns = campaignsRepository.getCampaignMetadata()

    fun selectCampaign(campaignId: Long) = campaignsRepository.setActiveCampaign(campaignId)

}