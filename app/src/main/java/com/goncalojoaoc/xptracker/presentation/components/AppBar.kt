package com.goncalojoaoc.xptracker.presentation.components

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.goncalojoaoc.xptracker.constants.NAVIGATION_ICON
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme

@Composable
fun Appbar(
    modifier: Modifier = Modifier,
    onNavIconPressed: () -> Unit = {},
    title: @Composable RowScope.() -> Unit,
    actions: @Composable RowScope.() -> Unit = {},
    navigationIcon: ImageVector = Icons.Default.Menu
) {
    TopAppBar(
        title = { Row { title() } },
        modifier = modifier,
        backgroundColor = MaterialTheme.colors.primary,
        elevation = 4.dp,
        actions = actions,
        navigationIcon = {
            IconButton(
                modifier = Modifier.testTag(NAVIGATION_ICON),
                onClick = onNavIconPressed) {
                Icon(navigationIcon, tint = MaterialTheme.colors.onPrimary, contentDescription = null)
            }
        }
    )
}

@Preview
@Composable
fun AppbarPreview() {
    DnDXPTrackerTheme(darkTheme = true) {
        Appbar(title = { Text(text = "Preview") })
    }
}