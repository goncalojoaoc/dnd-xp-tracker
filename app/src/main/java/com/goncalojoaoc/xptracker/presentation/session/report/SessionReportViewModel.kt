package com.goncalojoaoc.xptracker.presentation.session.report

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.goncalojoaoc.xptracker.data.ISessionsRepository
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterInSession
import com.goncalojoaoc.xptracker.model.session.report.IPlayerSessionReportMapper
import com.goncalojoaoc.xptracker.presentation.model.SessionIdentifier

class SessionReportViewModel @ViewModelInject constructor(
    private val sessionsRepo: ISessionsRepository,
    private val sessionReportMapper: IPlayerSessionReportMapper
) : ViewModel() {

    private val _sessionId = MutableLiveData<SessionIdentifier>()

    val session: LiveData<Session> = _sessionId.switchMap {
        sessionsRepo.getSession(it.campaignId, it.sessionNumber)
    }

    val charactersInSession: LiveData<List<CharacterInSession>> = _sessionId.switchMap {
        sessionsRepo.getCharactersInSession(it.campaignId, it.sessionNumber)
    }

    fun loadSession(sessionIdentifier: SessionIdentifier) {
        _sessionId.postValue(sessionIdentifier)
    }

    fun buildShareableXpReport(session: Session, characterInSession: CharacterInSession): String =
        sessionReportMapper.mapIndividual(session, characterInSession)

    fun buildFullShareableXpReport(): String {
        val session = session.value
        val characters = charactersInSession.value
        if (session != null && characters != null) {
            return sessionReportMapper.mapAll(session, characters)
        }
        return ""
    }
}
