package com.goncalojoaoc.xptracker.presentation.components.drawer

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Providers
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.goncalojoaoc.xptracker.R
import com.goncalojoaoc.xptracker.constants.ADD_CAMPAIGN
import com.goncalojoaoc.xptracker.model.campaign.CampaignMetadata
import com.goncalojoaoc.xptracker.presentation.components.getString
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme
import dev.chrisbanes.accompanist.insets.ProvideWindowInsets
import dev.chrisbanes.accompanist.insets.statusBarsHeight

interface NavigationDrawerContract {
    val campaigns: LiveData<List<CampaignMetadata>>

    fun selectCampaign(campaignId: Long)
    fun onNewCampaignClicked()
}

@Composable
fun ColumnScope.NavigationDrawer(
    drawerState: DrawerState = rememberDrawerState(initialValue = DrawerValue.Closed),
    model: NavigationDrawerContract
) {
    // Use statusBarsHeight() to add a spacer which pushes the drawer content
    // below the status bar (y-axis)
    Spacer(Modifier.statusBarsHeight())
    DrawerHeader()
    Divider()
    CampaignHeader {
        drawerState.close()
        model.onNewCampaignClicked()
    }

    val campaigns by model.campaigns.observeAsState()

    campaigns?.let {
        CampaignList(campaignItems = it, onCampaignClicked = { id ->
            model.selectCampaign(id)
            drawerState.close()
        })
    } ?: EmptyCampaigns()
}

@Composable
private fun DrawerHeader() {
    Row(
        modifier = Modifier.padding(horizontal = 16.dp).preferredHeight(56.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(text = getString(R.string.app_name))
    }
}

@Composable
private fun CampaignHeader(onNewCampaignClicked: () -> Unit) {
    Providers(AmbientContentAlpha provides ContentAlpha.medium) {
        Row(
            modifier = Modifier.padding(horizontal = 16.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                getString(stringId = R.string.campaigns_title),
                style = MaterialTheme.typography.body1,
                color = MaterialTheme.colors.onSurface,
                modifier = Modifier.weight(weight = 1f, fill = true)
            )
            IconButton(
                modifier = Modifier.testTag(ADD_CAMPAIGN),
                onClick = onNewCampaignClicked
            ) {
                Icon(Icons.Default.Add, tint = MaterialTheme.colors.onSurface, contentDescription = null)
            }

        }
    }
}

@Composable
private fun CampaignList(
    campaignItems: List<CampaignMetadata>,
    onCampaignClicked: (Long) -> Unit
) {
    LazyColumn(content = {
        items(campaignItems.size) { i ->
            val campaign = campaignItems[i]
            CampaignItem(
                text = campaign.name,
                selected = campaign.isActive,
                onCampaignClicked = { onCampaignClicked(campaign.id) })
        }
    })
}

@Composable
private fun CampaignItem(text: String, selected: Boolean, onCampaignClicked: () -> Unit) {
    val background = if (selected) {
        Modifier.background(MaterialTheme.colors.primary.copy(alpha = 0.2f))
    } else {
        Modifier
    }
    Row(
        modifier = Modifier
            .preferredHeight(52.dp)
            .fillMaxWidth()
            .then(background)
            .clip(MaterialTheme.shapes.medium)
            .clickable(onClick = onCampaignClicked),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Providers(AmbientContentAlpha provides ContentAlpha.medium) {
            Text(
                text,
                style = MaterialTheme.typography.body2,
                color = MaterialTheme.colors.onSurface,
                modifier = Modifier.padding(16.dp),
                maxLines = 1
            )
        }
    }
}

@Composable
private fun EmptyCampaigns() {
    Row(
        modifier = Modifier.padding(16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(text = getString(R.string.empty_campaigns_message))
    }
}

@Composable
@Preview
fun DrawerPreviewDark() {
    ProvideWindowInsets {
        DnDXPTrackerTheme(darkTheme = true) {
            Surface {
                Column {
                    NavigationDrawer(
                        model = object: NavigationDrawerContract {
                            override val campaigns: LiveData<List<CampaignMetadata>> = MutableLiveData(
                                listOf(
                                    CampaignMetadata(name = "Campaign 1")
                                )
                            )

                            override fun selectCampaign(campaignId: Long) {}

                            override fun onNewCampaignClicked() {}
                        }
                    )
                }
            }
        }
    }
}