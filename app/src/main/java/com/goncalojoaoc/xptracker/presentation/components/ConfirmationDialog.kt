package com.goncalojoaoc.xptracker.presentation.components

import androidx.annotation.StringRes
import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable

@Composable
fun ConfirmationDialog(
    showDialog: Boolean,
    setShowDialog: (Boolean) -> Unit,
    onConfirm: () -> Unit,
    @StringRes title: Int,
    @StringRes message: Int,
    @StringRes positiveButton: Int,
    @StringRes dismissButton: Int
) {
    if (showDialog) {
        AlertDialog(
            onDismissRequest = {},
            title = { Text(getString(title)) },
            confirmButton = {
                TextButton(
                    //colors = textButtonPositiveColors(),
                    onClick = {
                        setShowDialog(false)
                        onConfirm()
                    }) {
                    Text(getString(positiveButton))
                }
            },
            dismissButton = {
                TextButton(
                    //colors = textButtonNeutralColors(),
                    onClick = {
                        setShowDialog(false)
                    }) {
                    Text(getString(dismissButton))
                }
            },
            text = {
                Text(getString(message))
            }
        )
    }
}