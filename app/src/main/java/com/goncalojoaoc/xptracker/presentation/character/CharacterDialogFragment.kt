package com.goncalojoaoc.xptracker.presentation.character

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material.Surface
import androidx.compose.runtime.Providers
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.goncalojoaoc.xptracker.model.character.Character
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme
import com.goncalojoaoc.xptracker.utils.launch
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import dev.chrisbanes.accompanist.insets.AmbientWindowInsets
import dev.chrisbanes.accompanist.insets.ExperimentalAnimatedInsets
import dev.chrisbanes.accompanist.insets.ViewWindowInsetObserver
import kotlinx.coroutines.Dispatchers

@AndroidEntryPoint
class CharacterDialogFragment : BottomSheetDialogFragment() {

    companion object {
        private const val CHARACTER_ID = "CharacterId"
        private const val CAMPAIGN_ID = "CampaignId"
        private const val TAG = "CharacterDialogFragment"
        private const val NEW_CHARACTER = -1L

        fun show(
            campaignId: Long,
            characterId: Long = NEW_CHARACTER,
            fragmentManager: FragmentManager
        ) =
            CharacterDialogFragment().apply {
                arguments = Bundle().apply {
                    putLong(CHARACTER_ID, characterId)
                    putLong(CAMPAIGN_ID, campaignId)
                }
                show(fragmentManager, TAG)
            }
    }

    private val viewModel: CharacterDialogViewModel by viewModels()

    @ExperimentalAnimatedInsets
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            val windowInsets =
                ViewWindowInsetObserver(this).start(windowInsetsAnimationsEnabled = true)

            setContent {
                Providers(AmbientWindowInsets provides windowInsets) {
                    DnDXPTrackerTheme {
                        Surface {
                            CharacterDialog(model = CharacterDialogView())
                        }
                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let { args ->
            val campaignId = args.getLong(CAMPAIGN_ID)
            val characterId = args.getLong(CHARACTER_ID)
            if (characterId == NEW_CHARACTER) {
                viewModel.initWithEmptyCharacter(campaignId)
            } else {
                viewModel.initWithExistingCharacter(characterId)
            }
        }
    }

    private inner class CharacterDialogView : CharacterDialogContract {

        override val character: LiveData<Character> = viewModel.character

        override fun onNameChanged(name: String) {
            viewModel.onCharacterNameChanged(name)
        }

        override fun onXpChanged(xp: Int) {
            viewModel.onCharacterXpChanged(xp)
        }

        override fun save() {
            launch(Dispatchers.Default) {
                viewModel.save()
                launch(Dispatchers.Main) {
                    dismiss()
                }
            }
        }
    }
}
