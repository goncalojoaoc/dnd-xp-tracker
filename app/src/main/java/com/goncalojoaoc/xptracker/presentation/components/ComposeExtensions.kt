package com.goncalojoaoc.xptracker.presentation.components

import androidx.annotation.StringRes
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.AmbientContext

@Composable
fun getString(@StringRes stringId: Int): String = AmbientContext.current.getString(stringId)

@Composable
fun getString(@StringRes stringId: Int, vararg formatArgs: Any): String =
    AmbientContext.current.getString(stringId, *formatArgs)