package com.goncalojoaoc.xptracker.presentation.components

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import com.goncalojoaoc.xptracker.presentation.components.drawer.NavigationDrawer
import com.goncalojoaoc.xptracker.presentation.components.drawer.NavigationDrawerContract
import com.goncalojoaoc.xptracker.presentation.components.drawer.NavigationDrawerViewModel
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme

@Composable
fun XpTrackerScaffold(
    drawerModel: NavigationDrawerContract,
    scaffoldState: ScaffoldState = rememberScaffoldState(),
    content: @Composable (PaddingValues) -> Unit
) {
    DnDXPTrackerTheme {
        Scaffold(
            scaffoldState = scaffoldState,
            drawerContent = {
                NavigationDrawer(
                    drawerState = scaffoldState.drawerState,
                    model = drawerModel
                )
            },
            bodyContent = content
        )
    }
}