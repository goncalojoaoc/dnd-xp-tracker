package com.goncalojoaoc.xptracker.presentation.campaign.details

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.goncalojoaoc.xptracker.R
import com.goncalojoaoc.xptracker.constants.DROPDOWN_MENU
import com.goncalojoaoc.xptracker.model.campaign.Campaign
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.presentation.components.Appbar
import com.goncalojoaoc.xptracker.presentation.components.ConfirmationDialog
import com.goncalojoaoc.xptracker.presentation.components.characters.Characters
import com.goncalojoaoc.xptracker.presentation.components.getString
import com.goncalojoaoc.xptracker.presentation.session.list.SessionList
import com.goncalojoaoc.xptracker.presentation.session.list.formattedName
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme
import dev.chrisbanes.accompanist.insets.ProvideWindowInsets
import dev.chrisbanes.accompanist.insets.navigationBarsHeight
import dev.chrisbanes.accompanist.insets.statusBarsHeight
import java.util.*

interface CampaignDetailsContract {
    val campaign: LiveData<Campaign>
    val recentSessions: LiveData<List<Session>>

    fun onNavIconPressed()
    fun onNewSessionPressed()
    fun onOpenSessionPressed(session: Session)
    fun openSessionReport(session: Session)
    fun onEditCampaign(campaignId: Long)
    fun onDeleteCampaign()
    fun onNewCampaign()
    fun onViewPastSessions(campaignId: Long)
}

@Composable
fun CampaignDetails(
    campaignModel: CampaignDetailsContract
) {
    val campaignData by campaignModel.campaign.observeAsState()
    val sessions by campaignModel.recentSessions.observeAsState()

    Column(modifier = Modifier.fillMaxSize()) {
        campaignData?.let {
            Campaign(campaignModel = campaignModel, campaign = it, sessions = sessions)

            SessionButton(
                sessions = sessions,
                newSession = campaignModel::onNewSessionPressed,
                openSession = campaignModel::onOpenSessionPressed
            )

            Spacer(Modifier.navigationBarsHeight())
        } ?: NoCampaign(campaignModel)
    }
}

@Composable
private fun ColumnScope.Campaign(
    campaignModel: CampaignDetailsContract,
    campaign: Campaign,
    sessions: List<Session>?
) {
    Column(
        modifier = Modifier.weight(1f)
    ) {
        Spacer(Modifier.statusBarsHeight())

        Appbar(
            title = {
                Text(text = campaign.metadata.name)
            },
            onNavIconPressed = campaignModel::onNavIconPressed,
            actions = {
                var menuOpen by remember { mutableStateOf(false) }

                val iconButton = @Composable {
                    IconButton(
                        modifier = Modifier.testTag(DROPDOWN_MENU),
                        onClick = { menuOpen = true },
                    ) {
                        Icon(Icons.Default.MoreVert, tint = MaterialTheme.colors.onPrimary, contentDescription = null)
                    }
                }

                DropdownMenu(
                    toggle = iconButton,
                    expanded = menuOpen,
                    onDismissRequest = { menuOpen = false }
                ) {
                    DropdownMenuItem(onClick = {
                        campaignModel.onViewPastSessions(campaign.metadata.id)
                    }) {
                        Text(text = getString(R.string.campaign_see_all_sessions))
                    }

                    DropdownMenuItem(onClick = {
                        campaignModel.onEditCampaign(
                            campaign.metadata.id
                        )
                    }) {
                        Text(text = getString(R.string.campaign_edit_title))
                    }

                    val (showDialog, setShowDialog) = remember { mutableStateOf(false) }
                    DropdownMenuItem(onClick = { setShowDialog(true) }) {
                        Text(text = getString(R.string.campaign_delete))
                    }
                    ConfirmationDialog(
                        showDialog = showDialog,
                        setShowDialog = setShowDialog,
                        onConfirm = campaignModel::onDeleteCampaign,
                        title = R.string.campaign_delete_confirm_title,
                        message = R.string.campaign_delete_confirm_message,
                        positiveButton = R.string.campaign_delete_confirm_yes,
                        dismissButton = R.string.campaign_delete_confirm_no
                    )
                }
            }
        )

        Characters(
            characters = campaign.characters
        )

        SessionList(
            title = getString(R.string.recent_sessions_title),
            recentSessions = sessions,
            openSessionReport = campaignModel::openSessionReport,
            openSession = campaignModel::onOpenSessionPressed
        )
    }
}

@Composable
private fun ColumnScope.NoCampaign(campaignModel: CampaignDetailsContract) {
    Spacer(Modifier.statusBarsHeight())

    Appbar(
        title = {
            Text(text = getString(R.string.app_name))
        },
        onNavIconPressed = campaignModel::onNavIconPressed,
    )

    Column(
        modifier = Modifier.weight(1f),
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            modifier = Modifier
                .padding(16.dp),
            style = MaterialTheme.typography.body2,
            textAlign = TextAlign.Center,
            text = getString(R.string.campaign_empty)
        )

    }

    Button(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth()
            .preferredHeight(48.dp),
        onClick = campaignModel::onNewCampaign
    ) {
        Text(text = getString(R.string.campaign_create_button))
    }

    Spacer(Modifier.navigationBarsHeight())
}

@Composable
private fun SessionButton(
    sessions: List<Session>?,
    newSession: () -> Unit,
    openSession: (Session) -> Unit
) {
    val ongoing = sessions?.firstOrNull { it.inProgress }
    Button(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth()
            .preferredHeight(48.dp),
        onClick = { ongoing?.let(openSession) ?: newSession() }
    ) {
        Text(text = ongoing?.let { session ->
            getString(
                R.string.continue_ongoing_session,
                session.formattedName()
            )
        } ?: getString(R.string.new_session))
    }
}

@Composable
@Preview
fun Preview() {
    ProvideWindowInsets {
        DnDXPTrackerTheme(darkTheme = true) {
            Surface {
                CampaignDetails(
                    object : CampaignDetailsContract {
                        override val campaign: LiveData<Campaign>
                            get() = MutableLiveData(
                                /*Campaign(
                                    metadata = CampaignMetadata(
                                        name = "Bananas"
                                    ),
                                    characters = listOf(
                                        Character(
                                            campaignId = -1,
                                            name = "Dingus McFife"
                                        )
                                    )
                                )*/
                            )

                        override val recentSessions: LiveData<List<Session>>
                            get() = MutableLiveData(
                                listOf(
                                    Session(
                                        date = Date(),
                                        campaignId = -1,
                                        number = 1,
                                        inProgress = false
                                    )
                                )
                            )

                        override fun onNavIconPressed() {}
                        override fun onNewSessionPressed() {}
                        override fun onOpenSessionPressed(session: Session) {}
                        override fun openSessionReport(session: Session) {}
                        override fun onEditCampaign(campaignId: Long) {}
                        override fun onDeleteCampaign() {}
                        override fun onNewCampaign() {}
                        override fun onViewPastSessions(campaignId: Long) {}

                    }
                )
            }
        }
    }
}