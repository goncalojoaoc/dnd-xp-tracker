package com.goncalojoaoc.xptracker.presentation.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Providers
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.setContent
import androidx.compose.ui.viewinterop.AndroidViewBinding
import androidx.core.view.WindowCompat
import androidx.lifecycle.LiveData
import androidx.navigation.findNavController
import com.goncalojoaoc.xptracker.R
import com.goncalojoaoc.xptracker.databinding.ContentMainBinding
import com.goncalojoaoc.xptracker.model.campaign.CampaignMetadata
import com.goncalojoaoc.xptracker.presentation.campaign.details.CampaignDetailsFragmentDirections
import com.goncalojoaoc.xptracker.presentation.components.AmbientBackPressedDispatcher
import com.goncalojoaoc.xptracker.presentation.components.XpTrackerScaffold
import com.goncalojoaoc.xptracker.presentation.components.backPressHandler
import com.goncalojoaoc.xptracker.presentation.components.drawer.NavigationDrawerContract
import com.goncalojoaoc.xptracker.presentation.components.drawer.NavigationDrawerViewModel
import dagger.hilt.android.AndroidEntryPoint
import dev.chrisbanes.accompanist.insets.ProvideWindowInsets

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val viewModel: MainViewModel by viewModels()
    private val navigationDrawerViewModel: NavigationDrawerViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Turn off the decor fitting system windows, which allows us to handle insets,
        // including IME animations
        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            ProvideWindowInsets(consumeWindowInsets = false) {
                Providers(AmbientBackPressedDispatcher provides this) {
                    val scaffoldState = rememberScaffoldState()

                    val openDrawerEvent = viewModel.drawerShouldBeOpened.observeAsState()
                    if (openDrawerEvent.value == true) {
                        scaffoldState.drawerState.open {
                            viewModel.resetOpenDrawerAction()
                        }
                    }

                    // Intercepts back navigation when the drawer is open
                    backPressHandler(
                        enabled = scaffoldState.drawerState.isOpen,
                        onBackPressed = { scaffoldState.drawerState.close() },
                        highPriority = true
                    )

                    XpTrackerScaffold(
                        scaffoldState = scaffoldState,
                        drawerModel = NavigationDrawerView()
                    ) {
                        // Inflate the navigation container XML layout with View Binding
                        AndroidViewBinding(ContentMainBinding::inflate)
                    }
                }
            }
        }
    }

    private fun navigateToNewCampaign() {
        findNavController(R.id.nav_host_fragment).navigate(CampaignDetailsFragmentDirections.actionCampaignDetailsToCreateCampaignFragment())
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    private inner class NavigationDrawerView : NavigationDrawerContract {

        override val campaigns: LiveData<List<CampaignMetadata>> = navigationDrawerViewModel.campaigns

        override fun selectCampaign(campaignId: Long) {
            navigationDrawerViewModel.selectCampaign(campaignId)
        }

        override fun onNewCampaignClicked() {
            navigateToNewCampaign()
        }

    }
}
