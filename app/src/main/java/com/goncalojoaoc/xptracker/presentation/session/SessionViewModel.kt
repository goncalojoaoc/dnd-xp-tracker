package com.goncalojoaoc.xptracker.presentation.session

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.goncalojoaoc.xptracker.data.ICampaignsRepository
import com.goncalojoaoc.xptracker.data.ICharactersRepository
import com.goncalojoaoc.xptracker.data.ISessionsRepository
import com.goncalojoaoc.xptracker.model.mapper.EncounterXpToPartyMapper
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancementTable
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterInSession
import com.goncalojoaoc.xptracker.model.session.character_advancement.Encounter
import com.goncalojoaoc.xptracker.presentation.model.SessionIdentifier
import com.goncalojoaoc.xptracker.utils.await

class SessionViewModel @ViewModelInject constructor(
    private val sessionsRepo: ISessionsRepository,
    private val campaignsRepo: ICampaignsRepository,
    private val charactersRepository: ICharactersRepository
) : ViewModel() {

    private val sessionId: MutableLiveData<SessionIdentifier> = MutableLiveData()

    val session: LiveData<Session> = sessionId.switchMap {
        sessionsRepo.getSession(it.campaignId, it.sessionNumber)
    }

    val charactersInSession: LiveData<List<CharacterInSession>> = sessionId.switchMap {
        sessionsRepo.getCharactersInSession(it.campaignId, it.sessionNumber)
    }

    fun loadSession(id: SessionIdentifier) {
        sessionId.postValue(id)
    }

    suspend fun awardEncounterXp(encounterXp: Int) {
        charactersInSession.value?.let { characters ->
            val xpBump =
                Encounter(EncounterXpToPartyMapper.xpPerCharacter(encounterXp, characters.size))
            characters.forEach { it.advancement.add(xpBump) }
            sessionsRepo.updateAllCharactersInSession(characters)
        }
    }

    suspend fun awardEpicMoment(characterId: Long) {
        charactersInSession.value?.firstOrNull { it.characterId == characterId }?.let { character ->
            val epicMoment = CharacterAdvancementTable.calculateEpicMomentXp(
                modifier = campaignsRepo.getCampaignMetadata(character.campaignId)
                    .await().epicMomentXpModifier,
                characterXp = character.xpAfterSession
            )
            character.advancement.add(epicMoment)
            sessionsRepo.updateCharacterInSession(character)
        }
    }

    suspend fun endSession() {
        session.value?.let { session ->
            sessionsRepo.updateSession(
                session.copy(
                    inProgress = false
                )
            )
            val campaignCharacters =
                campaignsRepo.getCampaignById(session.campaignId).await().characters
            charactersInSession.value?.let { sessionCharacters ->
                charactersRepository.update(
                    CharacterAdvancementTable.advanceCharacters(
                        campaignCharacters,
                        sessionCharacters
                    )
                )
            }
        }
    }
}
