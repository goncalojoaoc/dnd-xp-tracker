package com.goncalojoaoc.xptracker.presentation.theme

import androidx.compose.ui.unit.dp

val cardElevation = 1.dp
val selectedCardElevation = 4.dp