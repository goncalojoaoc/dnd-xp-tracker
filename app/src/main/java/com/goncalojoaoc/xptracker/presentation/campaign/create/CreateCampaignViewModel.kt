package com.goncalojoaoc.xptracker.presentation.campaign.create

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.goncalojoaoc.xptracker.R
import com.goncalojoaoc.xptracker.data.ICampaignsRepository
import com.goncalojoaoc.xptracker.data.ICharactersRepository
import com.goncalojoaoc.xptracker.model.campaign.CampaignMetadata
import com.goncalojoaoc.xptracker.model.character.Character
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CreateCampaignViewModel @ViewModelInject constructor(
    @ApplicationContext private val context: Context,
    private val campaignRepo: ICampaignsRepository,
    private val charactersRepo: ICharactersRepository
) : ViewModel() {

    private val _campaignId = MutableLiveData<Long>()

    val isInCreationMode = MutableLiveData<Boolean>()

    val existingCampaign: LiveData<CampaignMetadata> = _campaignId.switchMap {
        campaignRepo.getCampaignMetadata(it)
    }

    val characters: LiveData<List<Character>> = _campaignId.switchMap {
        charactersRepo.getCharactersInCampaign(it)
    }

    private var cachedCampaign = buildEmptyCampaignMetadata()

    val campaignId: Long
        get() = cachedCampaign.id

    private val campaignObserver = Observer<CampaignMetadata> {
        cachedCampaign = it
    }

    init {
        existingCampaign.observeForever(campaignObserver)
    }

    override fun onCleared() {
        super.onCleared()
        existingCampaign.removeObserver(campaignObserver)
    }

    fun initWithEmptyCampaign() {
        GlobalScope.launch(Dispatchers.Default) {
            val id = campaignRepo.createCampaign(cachedCampaign)
            initWithExistingCampaign(id)
            isInCreationMode.postValue(true)
        }
    }

    fun initWithExistingCampaign(campaignId: Long) {
        isInCreationMode.postValue(false)
        _campaignId.postValue(campaignId)
    }

    fun onCampaignNameChanged(name: String) {
        cachedCampaign = cachedCampaign.copy(name = name)
    }

    fun onEpicMomentsModifierChanged(modifier: Int) {
        cachedCampaign = cachedCampaign.copy(epicMomentXpModifier = modifier)
    }

    suspend fun deleteCharacter(character: Character) {
        charactersRepo.delete(character)
    }

    suspend fun save() {
        campaignRepo.updateMetadata(cachedCampaign)
        campaignRepo.setActiveCampaign(cachedCampaign.id)
    }

    private fun buildEmptyCampaignMetadata() = CampaignMetadata(
        name = context.getString(R.string.campaign_default_new_name)
    )
}