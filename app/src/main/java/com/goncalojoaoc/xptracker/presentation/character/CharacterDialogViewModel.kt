package com.goncalojoaoc.xptracker.presentation.character

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.goncalojoaoc.xptracker.R
import com.goncalojoaoc.xptracker.data.ICharactersRepository
import com.goncalojoaoc.xptracker.model.character.Character
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CharacterDialogViewModel @ViewModelInject constructor(
    @ApplicationContext private val context: Context,
    private val characterRepo: ICharactersRepository
) : ViewModel() {

    private val _characterId = MutableLiveData<Long>()

    val character: LiveData<Character> = _characterId.switchMap {
        characterRepo.get(it)
    }

    private var cachedCharacter: Character? = null

    private val characterObserver = Observer<Character> {
        cachedCharacter = it
    }

    init {
        character.observeForever(characterObserver)
    }

    override fun onCleared() {
        super.onCleared()
        character.removeObserver(characterObserver)
    }

    fun initWithEmptyCharacter(campaignId: Long) {
        GlobalScope.launch(Dispatchers.Default) {
            cachedCharacter = buildEmptyCharacter(campaignId)
            val id = characterRepo.create(cachedCharacter!!)
            initWithExistingCharacter(id)
        }
    }

    fun initWithExistingCharacter(characterId: Long) {
        _characterId.postValue(characterId)
    }

    fun onCharacterNameChanged(name: String) {
        cachedCharacter = cachedCharacter?.copy(name = name)
    }

    fun onCharacterXpChanged(xp: Int) {
        cachedCharacter = cachedCharacter?.copy(xp = xp)
    }

    suspend fun save() {
        cachedCharacter?.let { character ->
            characterRepo.update(listOf(character))
        }
    }

    private fun buildEmptyCharacter(campaignId: Long) = Character(
        campaignId = campaignId,
        name = context.getString(R.string.character_default_name)
    )
}