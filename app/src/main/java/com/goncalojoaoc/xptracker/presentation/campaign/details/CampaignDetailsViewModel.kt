package com.goncalojoaoc.xptracker.presentation.campaign.details

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.goncalojoaoc.xptracker.data.ICampaignsRepository
import com.goncalojoaoc.xptracker.data.ICampaignsRepository.Companion.NO_ACTIVE_CAMPAIGN
import com.goncalojoaoc.xptracker.data.ISessionsRepository
import com.goncalojoaoc.xptracker.model.campaign.Campaign
import com.goncalojoaoc.xptracker.model.campaign.CampaignMetadata
import com.goncalojoaoc.xptracker.model.session.Session

class CampaignDetailsViewModel @ViewModelInject constructor(
    private val campaignsRepository: ICampaignsRepository,
    private val sessionsRepository: ISessionsRepository
) : ViewModel() {

    companion object {
        private const val RECENT_SESSIONS = 3
    }

    private val activeCampaign: LiveData<CampaignMetadata?> = campaignsRepository.getActiveCampaign()

    val campaign: LiveData<Campaign> = activeCampaign.switchMap { metadata ->
        campaignsRepository.getCampaignById(metadata?.id ?: NO_ACTIVE_CAMPAIGN)
    }

    val recentSessions: LiveData<List<Session>> = activeCampaign.switchMap { metadata ->
        sessionsRepository.getCampaignSessions(metadata?.id ?: NO_ACTIVE_CAMPAIGN, RECENT_SESSIONS)
    }

    suspend fun newSession(): Session? = activeCampaign.value?.let { activeCampaign ->
        sessionsRepository.createSessionInCampaign(activeCampaign.id)
    }

    suspend fun delete() {
        campaign.value?.let { campaign ->
            campaignsRepository.setActiveCampaign(NO_ACTIVE_CAMPAIGN)
            campaignsRepository.delete(campaign)
        }
    }

}
