package com.goncalojoaoc.xptracker.presentation.character

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.preferredHeight
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.goncalojoaoc.xptracker.R
import com.goncalojoaoc.xptracker.constants.CHARACTER_NAME
import com.goncalojoaoc.xptracker.constants.CURRENT_XP
import com.goncalojoaoc.xptracker.model.character.Character
import com.goncalojoaoc.xptracker.presentation.components.getString
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme
import dev.chrisbanes.accompanist.insets.ProvideWindowInsets
import dev.chrisbanes.accompanist.insets.navigationBarsWithImePadding

interface CharacterDialogContract {
    val character: LiveData<Character>

    fun onNameChanged(name: String)
    fun onXpChanged(xp: Int)
    fun save()
}

@Composable
fun CharacterDialog(model: CharacterDialogContract) {
    Column(
        Modifier
            .fillMaxWidth()
            .navigationBarsWithImePadding()
    ) {
        CharacterInfo(
            characterLiveData = model.character,
            onNameChanged = model::onNameChanged,
            onXpChanged = model::onXpChanged
        )

        SaveButton(save = model::save)
    }
}

@Composable
private fun CharacterInfo(
    characterLiveData: LiveData<Character>,
    onNameChanged: (String) -> Unit,
    onXpChanged: (Int) -> Unit
) {
    val character by characterLiveData.observeAsState()

    CharacterName(character = character, onNameChanged = onNameChanged)

    CharacterXp(character = character, onXpChanged = onXpChanged)
}

@Composable
private fun CharacterName(character: Character?, onNameChanged: (String) -> Unit) {
    var textState by remember { mutableStateOf(TextFieldValue(character?.name.orEmpty())) }

    OutlinedTextField(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .testTag(CHARACTER_NAME),
        value = textState,
        onValueChange = {
            textState = it
            onNameChanged(it.text)
        },
        keyboardOptions = KeyboardOptions(
            capitalization = KeyboardCapitalization.Words
        ),
        label = { Text(text = getString(R.string.character_name)) },
        activeColor = MaterialTheme.colors.onSurface
    )
}

@Composable
private fun CharacterXp(character: Character?, onXpChanged: (Int) -> Unit) {
    var textState by remember { mutableStateOf(TextFieldValue((character?.xp ?: 0).toString())) }
    val isValid = textState.text.trim().toIntOrNull() != null || textState.text.isEmpty()

    OutlinedTextField(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .testTag(CURRENT_XP),
        value = textState,
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Number
        ),
        onValueChange = {
            textState = it
            onXpChanged(it.text.trim().toIntOrNull() ?: 0)
        },
        isErrorValue = !isValid,
        label = { Text(text = getString(R.string.character_xp)) },
        activeColor = MaterialTheme.colors.onSurface
    )
}

@Composable
private fun SaveButton(save: () -> Unit) {
    Button(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .preferredHeight(48.dp),
        onClick = save
    ) {
        Text(text = getString(R.string.character_save))
    }
}

@Composable
@Preview
fun Preview() {
    ProvideWindowInsets {
        DnDXPTrackerTheme(darkTheme = true) {
            Surface {
                CharacterDialog(object : CharacterDialogContract {
                    override val character: LiveData<Character> = MutableLiveData(
                        Character(
                            name = "Dingus Dingleberry II",
                            xp = 56400,
                            campaignId = 0
                        )
                    )

                    override fun onNameChanged(name: String) {}

                    override fun onXpChanged(xp: Int) {}

                    override fun save() {}
                })
            }
        }
    }
}