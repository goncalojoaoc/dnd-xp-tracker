package com.goncalojoaoc.xptracker.presentation.components.characters

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Providers
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.unit.dp
import com.goncalojoaoc.xptracker.R
import com.goncalojoaoc.xptracker.constants.ADD_CHARACTER
import com.goncalojoaoc.xptracker.constants.DELETE_CHARACTER
import com.goncalojoaoc.xptracker.constants.EDIT_CHARACTER
import com.goncalojoaoc.xptracker.model.character.Character
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancementTable.level
import com.goncalojoaoc.xptracker.presentation.components.ConfirmationDialog
import com.goncalojoaoc.xptracker.presentation.components.getString
import com.goncalojoaoc.xptracker.presentation.theme.cardElevation

@Composable
fun Characters(
    characters: List<Character>?,
    addCharacter: (() -> Unit)? = null,
    editCharacter: ((Character) -> Unit)? = null,
    deleteCharacter: ((Character) -> Unit)? = null
) {
    Row(
        modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            modifier = Modifier.weight(1f, true),
            style = MaterialTheme.typography.h5,
            text = getString(R.string.characters_title)
        )

        if (addCharacter != null) {
            IconButton(
                modifier = Modifier.testTag(ADD_CHARACTER),
                onClick = addCharacter
            ) {
                Icon(Icons.Default.Add, contentDescription = null)
            }
        }
    }

    LazyColumn(
        modifier = Modifier.padding(horizontal = 16.dp),
        content = {
            items(characters?.size ?: 0) { i ->
                characters?.let {
                    CharacterDetails(
                        character = characters[i],
                        editCharacter = editCharacter,
                        deleteCharacter = deleteCharacter
                    )
                }
            }
        }
    )
}

@Composable
fun CharacterDetails(
    character: Character,
    editCharacter: ((Character) -> Unit)?,
    deleteCharacter: ((Character) -> Unit)?
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 4.dp),
        elevation = cardElevation
    ) {
        ConstraintLayout(
            modifier = Modifier.padding(8.dp)
        ) {
            val (name, level, xp, line, delete, edit) = createRefs()

            Text(
                modifier = Modifier.constrainAs(name) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(delete.start)
                    bottom.linkTo(level.top)
                    width = Dimension.fillToConstraints
                },
                maxLines = 1,
                style = MaterialTheme.typography.body1,
                text = character.name
            )

            Providers(AmbientContentAlpha provides ContentAlpha.medium) {
                Text(
                    modifier = Modifier.constrainAs(level) {
                        top.linkTo(name.bottom, margin = 4.dp)
                        start.linkTo(parent.start)
                        bottom.linkTo(parent.bottom)
                    },
                    style = MaterialTheme.typography.caption,
                    text = getString(R.string.level, character.level)
                )

                Spacer(
                    modifier = Modifier
                        .preferredWidth(1.dp)
                        .background(color = MaterialTheme.colors.onSurface.copy(0.12f))
                        .constrainAs(line) {
                            height = Dimension.fillToConstraints
                            top.linkTo(name.bottom, margin = 4.dp)
                            bottom.linkTo(parent.bottom)
                            start.linkTo(level.end, margin = 8.dp)
                        }
                )

                Text(
                    modifier = Modifier.constrainAs(xp) {
                        top.linkTo(level.top)
                        bottom.linkTo(level.bottom)
                        start.linkTo(line.end, margin = 8.dp)
                    },
                    style = MaterialTheme.typography.caption,
                    text = getString(R.string.total_xp, character.xp)
                )
            }

            if (deleteCharacter != null) {
                val (showDialog, setShowDialog) = remember { mutableStateOf(false) }
                ConfirmationDialog(
                    showDialog = showDialog,
                    setShowDialog = setShowDialog,
                    onConfirm = { deleteCharacter(character) },
                    title = R.string.character_delete_confirm_title,
                    message = R.string.character_delete_confirm_text,
                    positiveButton = R.string.character_delete_confirm_yes,
                    dismissButton = R.string.character_delete_confirm_no
                )

                IconButton(
                    modifier = Modifier.constrainAs(delete) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        end.linkTo(edit.start)
                    }.testTag(DELETE_CHARACTER),
                    onClick = { setShowDialog(true) }
                ) {
                    Icon(Icons.Default.Delete, tint = MaterialTheme.colors.onSurface, contentDescription = null)
                }
            }

            if (editCharacter != null) {
                IconButton(
                    modifier = Modifier.constrainAs(edit) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        end.linkTo(parent.end)
                    }.testTag(EDIT_CHARACTER),
                    onClick = { editCharacter(character) }
                ) {
                    Icon(Icons.Default.Edit, tint = MaterialTheme.colors.onSurface, contentDescription = null)
                }
            }
        }
    }
}
