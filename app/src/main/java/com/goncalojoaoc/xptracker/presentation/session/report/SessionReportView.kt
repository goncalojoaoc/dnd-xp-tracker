package com.goncalojoaoc.xptracker.presentation.session.report

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.filled.Share
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.goncalojoaoc.xptracker.R
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.model.session.character_advancement.*
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancementTable.hasLeveledUp
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancementTable.levelAfterSession
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancementTable.levelBeforeSession
import com.goncalojoaoc.xptracker.presentation.components.Appbar
import com.goncalojoaoc.xptracker.presentation.components.getString
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme
import com.goncalojoaoc.xptracker.presentation.theme.cardElevation
import com.goncalojoaoc.xptracker.presentation.theme.selectedCardElevation
import dev.chrisbanes.accompanist.insets.ProvideWindowInsets
import dev.chrisbanes.accompanist.insets.navigationBarsPadding
import dev.chrisbanes.accompanist.insets.statusBarsHeight
import java.text.SimpleDateFormat
import java.util.*

interface SessionReportContract {
    val session: LiveData<Session>
    val characters: LiveData<List<CharacterInSession>>

    fun onNavIconPressed()
    fun share(session: Session, character: CharacterInSession)
    fun shareAll()
}

@Composable
fun SessionReport(model: SessionReportContract) {
    val session by model.session.observeAsState()

    Column(
        Modifier
            .fillMaxSize()
            .navigationBarsPadding()
    ) {

        Spacer(Modifier.statusBarsHeight())

        AppBarWithMenu(session, model::onNavIconPressed, model::shareAll)

        Characters(charactersInSession = model.characters, share = { character ->
            session?.let { model.share(it, character) }
        })

        session?.let {
            Text(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(),
                style = MaterialTheme.typography.body2,
                textAlign = TextAlign.End,
                text = getString(R.string.report_session_date, it.date.format())
            )
        }
    }
}

@Composable
private fun AppBarWithMenu(session: Session?, onNavIconPressed: () -> Unit, shareAll: () -> Unit) {
    Appbar(
        title = {
            Text(text = session?.reportTitle().orEmpty())
        },
        actions = {
            var menuOpen by remember { mutableStateOf(false) }

            val iconButton = @Composable {
                IconButton(
                    onClick = { menuOpen = true },
                ) {
                    Icon(Icons.Default.MoreVert, tint = MaterialTheme.colors.onPrimary, contentDescription = null)
                }
            }

            DropdownMenu(
                toggle = iconButton,
                expanded = menuOpen,
                onDismissRequest = { menuOpen = false }) {
                DropdownMenuItem(onClick = shareAll) {
                    Text(text = getString(R.string.report_share_all))
                }
            }
        },
        navigationIcon = Icons.Default.ArrowBack,
        onNavIconPressed = onNavIconPressed
    )
}

@Composable
private fun ColumnScope.Characters(
    charactersInSession: LiveData<List<CharacterInSession>>,
    share: (CharacterInSession) -> Unit
) {
    val characters by charactersInSession.observeAsState()

    LazyColumn(
        modifier = Modifier.weight(1f),
        content = {
            items(characters?.size ?: 0) { i ->
                characters?.let { list ->
                    val character = list[i]
                    Character(
                        character = character,
                        onShare = { share(character) }
                    )
                }
            }
        }
    )
}

@Composable
private fun Character(character: CharacterInSession, onShare: () -> Unit) {
    var expanded by remember { mutableStateOf(false) }

    Card(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
            .padding(top = 16.dp)
            .clickable(onClick = { expanded = !expanded }),
        elevation = if (expanded) selectedCardElevation else cardElevation
    ) {
        ConstraintLayout(
            Modifier
                .padding(8.dp)
                .animateContentSize()
        ) {
            val (name, level, xp, share, details) = createRefs()

            Text(
                modifier = Modifier.constrainAs(name) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(share.start)
                    width = Dimension.fillToConstraints
                },
                text = character.name,
                style = MaterialTheme.typography.h6,
                fontWeight = FontWeight.Bold
            )

            Level(modifier = Modifier.constrainAs(level) {
                top.linkTo(name.bottom)
                end.linkTo(name.end)
                start.linkTo(parent.start)
                bottom.linkTo(xp.top)
                width = Dimension.fillToConstraints
            }, character = character)

            Text(
                modifier = Modifier.constrainAs(xp) {
                    top.linkTo(level.bottom)
                    end.linkTo(name.end)
                    start.linkTo(parent.start)
                    bottom.linkTo(details.top)
                    width = Dimension.fillToConstraints
                },
                text = getString(R.string.report_xp_this_session, character.sessionXp),
                style = MaterialTheme.typography.body2
            )

            IconButton(
                modifier = Modifier.constrainAs(share) {
                    start.linkTo(name.end)
                    end.linkTo(parent.end)
                    top.linkTo(parent.top)
                    bottom.linkTo(xp.bottom)
                },
                onClick = onShare
            ) {
                Icon(Icons.Default.Share, tint = MaterialTheme.colors.onSurface, contentDescription = null)
            }

            if (expanded) {
                XpDetails(
                    modifier = Modifier.constrainAs(details) {
                        top.linkTo(xp.bottom)
                        start.linkTo(parent.start)
                    },
                    characterAdvancement = character.advancement
                )
            }
        }
    }
}

@Composable
private fun XpDetails(modifier: Modifier, characterAdvancement: CharacterAdvancement) {
    Column(modifier = modifier) {
        if (characterAdvancement.isNotEmpty()) {
            Divider(modifier = Modifier.padding(vertical = 8.dp))
        }

        if (characterAdvancement.encounters.isNotEmpty()) {
            Text(
                text = getString(
                    R.string.report_encounters_title,
                    characterAdvancement.encountersCount,
                    characterAdvancement.encountersXp
                ),
                style = MaterialTheme.typography.body2
            )

            characterAdvancement.encounters.forEach { encounter ->
                Text(
                    text = xpBumpLabel(encounter),
                    style = MaterialTheme.typography.caption
                )
            }
        }

        if (characterAdvancement.epicMoments.isNotEmpty()) {
            Text(
                modifier = Modifier.padding(top = 4.dp),
                text = getString(
                    R.string.report_epic_moments_title,
                    characterAdvancement.epicMomentsCount,
                    characterAdvancement.epicMomentsXp
                ),
                style = MaterialTheme.typography.body2
            )
        }

        if (characterAdvancement.others.isNotEmpty()) {
            Text(
                modifier = Modifier.padding(top = 4.dp),
                text = getString(R.string.report_others_title, characterAdvancement.othersXp),
                style = MaterialTheme.typography.body2
            )

            characterAdvancement.others.forEach { other ->
                Text(
                    text = xpBumpLabel(other),
                    style = MaterialTheme.typography.caption
                )
            }
        }
    }
}

@Composable
private fun xpBumpLabel(xpBump: XpBump): String = when (xpBump) {
    is Encounter -> getString(R.string.report_encounter, xpBump.awardedXp)
    is EpicMoment -> getString(R.string.report_epic_moment, xpBump.awardedXp)
    is Other -> getString(R.string.report_other, xpBump.description, xpBump.awardedXp)
}

@Composable
private fun Level(modifier: Modifier, character: CharacterInSession) {
    if (character.hasLeveledUp) {
        Row(modifier = modifier, horizontalArrangement = Arrangement.Start) {
            Text(
                modifier = Modifier.padding(end = 4.dp),
                text = getString(R.string.character_level, character.levelBeforeSession),
                style = MaterialTheme.typography.body2
            )
            Image(
                modifier = Modifier.padding(end = 4.dp),
                imageVector = vectorResource(R.drawable.ic_double_arrow),
                colorFilter = ColorFilter.tint(MaterialTheme.colors.secondary),
                contentDescription = null
            )
            Text(
                text = getString(R.string.character_level, character.levelAfterSession),
                style = MaterialTheme.typography.body2
            )
        }
    } else {
        Text(
            modifier = modifier,
            text = getString(R.string.character_level, character.levelAfterSession)
        )
    }
}

@Composable
private fun Session.reportTitle(): String =
    getString(
        R.string.report_title,
        name ?: getString(R.string.session_default_name, number)
    )

@Composable
private fun Date.format(): String =
    SimpleDateFormat("yyyy/MM/dd - HH:mm", Locale.getDefault()).format(this)

@Composable
@Preview
fun Preview() {
    ProvideWindowInsets {
        DnDXPTrackerTheme(darkTheme = true) {
            Surface {
                SessionReport(object : SessionReportContract {
                    override val session: LiveData<Session> = MutableLiveData(
                        Session(
                            date = Date(),
                            inProgress = false,
                            number = 1,
                            campaignId = 1
                        )
                    )
                    override val characters: LiveData<List<CharacterInSession>> = MutableLiveData(
                        listOf(
                            CharacterInSession(
                                characterId = 1,
                                campaignId = 1,
                                sessionNumber = 1,
                                name = "Dingus",
                                startingXp = 0,
                                advancement = CharacterAdvancement(
                                    Encounter(200),
                                    EpicMoment(15),
                                    EpicMoment(15),
                                    Encounter(150),
                                    Other(5, "Extra boost for RPing an intelligence of 4")
                                )
                            )
                        )
                    )

                    override fun onNavIconPressed() {}
                    override fun share(session: Session, character: CharacterInSession) {}
                    override fun shareAll() {}
                })
            }
        }
    }
}