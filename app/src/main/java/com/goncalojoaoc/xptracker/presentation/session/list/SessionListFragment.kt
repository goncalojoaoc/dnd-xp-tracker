package com.goncalojoaoc.xptracker.presentation.session.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material.Surface
import androidx.compose.runtime.Providers
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme
import dagger.hilt.android.AndroidEntryPoint
import dev.chrisbanes.accompanist.insets.AmbientWindowInsets
import dev.chrisbanes.accompanist.insets.ViewWindowInsetObserver

@AndroidEntryPoint
class SessionListFragment : Fragment() {

    private val viewModel: SessionListViewModel by viewModels()

    private val args: SessionListFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

            val windowInsets = ViewWindowInsetObserver(this).start()

            setContent {
                Providers(AmbientWindowInsets provides windowInsets) {
                    DnDXPTrackerTheme {
                        Surface {
                            Sessions(SessionListView())
                        }
                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.loadSessions(args.campaignId)
    }

    private inner class SessionListView : SessionListContract {

        override val sessions: LiveData<List<Session>> = viewModel.sessions

        override fun openSessionReport(session: Session) {
            findNavController().navigate(
                SessionListFragmentDirections.actionSessionListFragmentToSessionReportFragment(
                    session.number,
                    session.campaignId
                )
            )
        }

        override fun openOngoingSession(session: Session) {
            findNavController().navigate(
                SessionListFragmentDirections.actionSessionListFragmentToSessionFragment(
                    session.number,
                    session.campaignId
                )
            )
        }

        override fun onNavIconPressed() {
            findNavController().navigateUp()
        }

    }
}