package com.goncalojoaoc.xptracker.presentation.campaign.create

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material.Surface
import androidx.compose.runtime.Providers
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.goncalojoaoc.xptracker.model.campaign.CampaignMetadata
import com.goncalojoaoc.xptracker.model.character.Character
import com.goncalojoaoc.xptracker.presentation.character.CharacterDialogFragment
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme
import com.goncalojoaoc.xptracker.utils.hideKeyboard
import com.goncalojoaoc.xptracker.utils.launch
import dagger.hilt.android.AndroidEntryPoint
import dev.chrisbanes.accompanist.insets.AmbientWindowInsets
import dev.chrisbanes.accompanist.insets.ExperimentalAnimatedInsets
import dev.chrisbanes.accompanist.insets.ViewWindowInsetObserver
import kotlinx.coroutines.Dispatchers

@AndroidEntryPoint
class CreateCampaignFragment : Fragment() {

    companion object {
        private const val NEW_CAMPAIGN = -1L
    }

    private val args: CreateCampaignFragmentArgs by navArgs()

    private val viewModel: CreateCampaignViewModel by viewModels()

    @ExperimentalAnimatedInsets
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

            val windowInsets =
                ViewWindowInsetObserver(this).start(windowInsetsAnimationsEnabled = true)

            setContent {
                Providers(AmbientWindowInsets provides windowInsets) {
                    DnDXPTrackerTheme {
                        Surface {
                            CreateCampaignView(model = CreateCampaignView())
                        }
                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (args.campaignId == NEW_CAMPAIGN) {
            viewModel.initWithEmptyCampaign()
        } else {
            viewModel.initWithExistingCampaign(args.campaignId)
        }
    }

    private inner class CreateCampaignView : CreateCampaignContract {
        override val campaign: LiveData<CampaignMetadata> = viewModel.existingCampaign
        override val characters: LiveData<List<Character>> = viewModel.characters
        override val newCampaign: LiveData<Boolean> = viewModel.isInCreationMode

        override fun onNavIconPressed() {
            findNavController().navigateUp()
        }

        override fun onCampaignNameChanged(name: String) {
            viewModel.onCampaignNameChanged(name)
        }

        override fun onEpicMomentModifierChanged(modifier: Int) {
            viewModel.onEpicMomentsModifierChanged(modifier)
        }

        override fun addCharacter() {
            CharacterDialogFragment.show(
                campaignId = viewModel.campaignId,
                fragmentManager = parentFragmentManager
            )
        }

        override fun editCharacter(character: Character) {
            CharacterDialogFragment.show(
                campaignId = viewModel.campaignId,
                characterId = character.id,
                fragmentManager = parentFragmentManager
            )
        }

        override fun deleteCharacter(character: Character) {
            launch(Dispatchers.Default) {
                viewModel.deleteCharacter(character)
            }
        }

        override fun save() {
            activity?.hideKeyboard()
            launch(Dispatchers.Default) {
                viewModel.save()
                launch(Dispatchers.Main) {
                    findNavController().popBackStack()
                }
            }
        }
    }
}