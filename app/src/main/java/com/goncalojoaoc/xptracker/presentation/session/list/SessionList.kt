package com.goncalojoaoc.xptracker.presentation.session.list

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Providers
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.goncalojoaoc.xptracker.R
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.presentation.components.Appbar
import com.goncalojoaoc.xptracker.presentation.components.getString
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme
import com.goncalojoaoc.xptracker.presentation.theme.cardElevation
import dev.chrisbanes.accompanist.insets.ProvideWindowInsets
import dev.chrisbanes.accompanist.insets.navigationBarsHeight
import dev.chrisbanes.accompanist.insets.statusBarsHeight
import java.text.SimpleDateFormat
import java.util.*

interface SessionListContract {
    val sessions: LiveData<List<Session>>

    fun openSessionReport(session: Session)
    fun openOngoingSession(session: Session)
    fun onNavIconPressed()
}

@Composable
fun Sessions(model: SessionListContract) {
    val sessions by model.sessions.observeAsState()

    Column(Modifier.fillMaxSize()) {
        Spacer(Modifier.statusBarsHeight())

        Appbar(
            modifier = Modifier.padding(bottom = 16.dp),
            title = { Text(getString(R.string.sessions_title)) },
            onNavIconPressed = model::onNavIconPressed,
            navigationIcon = Icons.Default.ArrowBack
        )

        SessionList(
            recentSessions = sessions,
            openSessionReport = model::openSessionReport,
            openSession = model::openOngoingSession
        )

        Spacer(Modifier.navigationBarsHeight())
    }
}

@Composable
fun SessionList(
    title: String? = null,
    recentSessions: List<Session>?,
    openSessionReport: (Session) -> Unit,
    openSession: (Session) -> Unit
) {
    title?.let {
        Text(
            modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
            style = MaterialTheme.typography.h5,
            text = it
        )
    }
    if (recentSessions.isNullOrEmpty()) {
        EmptySessions()
    } else {
        LazyColumn(
            modifier = Modifier.padding(horizontal = 16.dp),
            content = {
                items(recentSessions.size) { i ->
                    val session = recentSessions[i]
                    SessionDetails(
                        session = session,
                        openSessionReport = { openSessionReport(session) },
                        openSession = { openSession(session) }
                    )
                }
            }
        )
    }
}

@Composable
private fun SessionDetails(
    session: Session,
    openSessionReport: () -> Unit,
    openSession: () -> Unit
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 4.dp)
            .clickable(onClick = if (session.inProgress) openSession else openSessionReport),
        elevation = cardElevation
    ) {
        ConstraintLayout(modifier = Modifier.padding(8.dp)) {
            val (text, label) = createRefs()
            Column(modifier = Modifier.constrainAs(text) {
                start.linkTo(parent.start)
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                end.linkTo(label.start)
                width = Dimension.fillToConstraints
            }) {
                Text(
                    style = MaterialTheme.typography.body1,
                    text = session.formattedName()
                )

                Providers(AmbientContentAlpha provides ContentAlpha.medium) {
                    Text(
                        style = MaterialTheme.typography.caption,
                        text = session.date.format()
                    )
                }
            }

            if (session.inProgress) {
                Text(
                    modifier = Modifier
                        .constrainAs(label) {
                            top.linkTo(parent.top)
                            end.linkTo(parent.end)
                            start.linkTo(text.end)
                            bottom.linkTo(parent.bottom)
                        }
                        .background(
                            shape = MaterialTheme.shapes.small,
                            color = MaterialTheme.colors.primary
                        )
                        .padding(4.dp),
                    text = getString(R.string.session_in_progress),
                    style = MaterialTheme.typography.caption,
                    color = MaterialTheme.colors.onPrimary
                )
            }
        }
    }
}

@Composable
private fun EmptySessions() {
    Providers(AmbientContentAlpha provides ContentAlpha.medium) {
        Text(
            modifier = Modifier.padding(horizontal = 16.dp),
            style = MaterialTheme.typography.caption,
            text = getString(R.string.empty_sessions)
        )
    }
}

@Composable
fun Session.formattedName(): String =
    name ?: getString(R.string.session_default_name, number)

@Composable
private fun Date.format(): String =
    SimpleDateFormat("yyyy/MM/dd - HH:mm", Locale.getDefault()).format(this)

@Composable
@Preview
fun Preview() {
    ProvideWindowInsets {
        DnDXPTrackerTheme(darkTheme = true) {
            Surface {
                Sessions(model = object : SessionListContract {
                    override val sessions: LiveData<List<Session>>
                        get() = MutableLiveData(listOf(
                            Session(
                                date = Date(),
                                inProgress = true,
                                number = 2,
                                campaignId = 1
                            ),
                            Session(
                                date = Date(),
                                inProgress = false,
                                number = 1,
                                campaignId = 1
                            )
                        ))

                    override fun openSessionReport(session: Session) {}
                    override fun openOngoingSession(session: Session) {}
                    override fun onNavIconPressed() {}
                })
            }
        }
    }
}
