package com.goncalojoaoc.xptracker.presentation.session.report

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material.Surface
import androidx.compose.runtime.Providers
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterInSession
import com.goncalojoaoc.xptracker.presentation.model.SessionIdentifier
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme
import dagger.hilt.android.AndroidEntryPoint
import dev.chrisbanes.accompanist.insets.AmbientWindowInsets
import dev.chrisbanes.accompanist.insets.ViewWindowInsetObserver

@AndroidEntryPoint
class SessionReportFragment : Fragment() {

    private val args: SessionReportFragmentArgs by navArgs()

    private val viewModel: SessionReportViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

            val windowInsets = ViewWindowInsetObserver(this).start()

            setContent {
                Providers(AmbientWindowInsets provides windowInsets) {
                    DnDXPTrackerTheme {
                        Surface {
                            SessionReport(model = SessionReportView())
                        }
                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.loadSession(
            SessionIdentifier(
                sessionNumber = args.sessionNumber,
                campaignId = args.campaignId
            )
        )
    }

    private fun share(message: String) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, message)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }

    private inner class SessionReportView : SessionReportContract {
        override val session: LiveData<Session> = viewModel.session
        override val characters: LiveData<List<CharacterInSession>> = viewModel.charactersInSession

        override fun onNavIconPressed() {
            findNavController().navigateUp()
        }

        override fun share(session: Session, character: CharacterInSession) {
            share(viewModel.buildShareableXpReport(session, character))
        }

        override fun shareAll() {
            share(viewModel.buildFullShareableXpReport())
        }

    }
}