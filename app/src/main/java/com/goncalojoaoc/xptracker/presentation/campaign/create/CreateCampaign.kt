package com.goncalojoaoc.xptracker.presentation.campaign.create

import androidx.compose.foundation.ScrollableColumn
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.goncalojoaoc.xptracker.R
import com.goncalojoaoc.xptracker.model.campaign.CampaignMetadata
import com.goncalojoaoc.xptracker.model.character.Character
import com.goncalojoaoc.xptracker.presentation.components.Appbar
import com.goncalojoaoc.xptracker.presentation.components.characters.Characters
import com.goncalojoaoc.xptracker.presentation.components.getString
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme
import dev.chrisbanes.accompanist.insets.ProvideWindowInsets
import dev.chrisbanes.accompanist.insets.navigationBarsWithImePadding
import dev.chrisbanes.accompanist.insets.statusBarsHeight

interface CreateCampaignContract {
    val campaign: LiveData<CampaignMetadata>
    val characters: LiveData<List<Character>>
    val newCampaign: LiveData<Boolean>

    fun onNavIconPressed()
    fun onCampaignNameChanged(name: String)
    fun onEpicMomentModifierChanged(modifier: Int)
    fun addCharacter()
    fun editCharacter(character: Character)
    fun deleteCharacter(character: Character)
    fun save()
}

@Composable
fun CreateCampaignView(model: CreateCampaignContract) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .navigationBarsWithImePadding()
    ) {
        Spacer(Modifier.statusBarsHeight())

        Appbar(
            navigationIcon = Icons.Default.ArrowBack,
            title = {
                val isNewCampaign by model.newCampaign.observeAsState()
                Text(
                    text = getString(
                        if (isNewCampaign == true) {
                            R.string.campaign_create_title
                        } else {
                            R.string.campaign_edit_title
                        }
                    )
                )
            },
            actions = {
                IconButton(onClick = model::save) {
                    Icon(Icons.Default.Check, tint = MaterialTheme.colors.onPrimary, contentDescription = null)
                }
            },
            onNavIconPressed = model::onNavIconPressed
        )

        val campaign by model.campaign.observeAsState()
        CampaignName(campaign?.name.orEmpty(), model::onCampaignNameChanged)

        EpicMomentsModifier(campaign?.epicMomentXpModifier ?: 0, model::onEpicMomentModifierChanged)

        Spacer(Modifier.height(8.dp))

        val characterList by model.characters.observeAsState()
        Characters(
            characters = characterList,
            addCharacter = model::addCharacter,
            editCharacter = model::editCharacter,
            deleteCharacter = model::deleteCharacter
        )
    }
}

@Composable
private fun CampaignName(name: String, onNameChanged: (String) -> Unit) {
    var textState by remember {
        mutableStateOf(
            TextFieldValue(name)
        )
    }
    val isValid = textState.text.isNotBlank()
    if (!isValid) textState = TextFieldValue(name)

    Row(modifier = Modifier.padding(16.dp)) {
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            value = textState,
            onValueChange = {
                textState = it
                onNameChanged(it.text)
            },
            isErrorValue = !isValid,
            label = { Text(text = getString(R.string.campaign_name_label)) },
            activeColor = MaterialTheme.colors.onSurface
        )
    }
}

@Composable
private fun EpicMomentsModifier(modifier: Int, onEpicMomentModifierChanged: (Int) -> Unit) {
    var expanded by remember { mutableStateOf(false) }

    ConstraintLayout(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
    ) {
        val (label, arrow) = createRefs()
        Text(
            modifier = Modifier.constrainAs(label) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                bottom.linkTo(parent.bottom)
            },
            text = getString(R.string.campaign_epic_moments_label),
            style = MaterialTheme.typography.body1
        )

        IconButton(
            modifier = Modifier.constrainAs(arrow) {
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                end.linkTo(parent.end)
            },
            onClick = { expanded = !expanded }) {
            Icon(if (expanded) Icons.Default.ExpandLess else Icons.Default.ExpandMore, contentDescription = null)
        }

    }
    if (expanded) {
        ScrollableColumn {
            Text(
                modifier = Modifier.padding(horizontal = 16.dp).padding(bottom = 8.dp),
                text = getString(R.string.campaign_epic_moments_modifier_description),
                style = MaterialTheme.typography.body2,
                lineHeight = 20.sp
            )
        }
    }
    var epicMoments by remember {
        mutableStateOf(TextFieldValue(modifier.toString()))
    }
    val isValid = epicMoments.text.toIntOrNull() != null
    if (!isValid || epicMoments.text.toInt() == 0) epicMoments = TextFieldValue(modifier.toString())
    OutlinedTextField(
        modifier = Modifier
            .padding(horizontal = 16.dp)
            .fillMaxWidth(),
        value = epicMoments,
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Number
        ),
        isErrorValue = !isValid,
        activeColor = MaterialTheme.colors.onSurface,
        onValueChange = {
            epicMoments = it
            epicMoments.text.toIntOrNull()?.let(onEpicMomentModifierChanged)
        },
        label = { Text(getString(R.string.campaign_epic_moments_modifier_label)) }
    )
}

@Composable
@Preview
fun Preview() {
    ProvideWindowInsets {
        DnDXPTrackerTheme(darkTheme = true) {
            Surface {
                CreateCampaignView(
                    object : CreateCampaignContract {
                        override val campaign =
                            MutableLiveData(CampaignMetadata(name = "New Campaign"))
                        override val characters: LiveData<List<Character>> =
                            MutableLiveData(
                                listOf(
                                    Character(
                                        campaignId = 0,
                                        name = "New Character"
                                    )
                                )
                            )
                        override val newCampaign = MutableLiveData(true)

                        override fun onNavIconPressed() {}
                        override fun onCampaignNameChanged(name: String) {}
                        override fun onEpicMomentModifierChanged(modifier: Int) {}
                        override fun addCharacter() {}
                        override fun editCharacter(character: Character) {}
                        override fun deleteCharacter(character: Character) {}
                        override fun save() {}
                    }
                )
            }
        }
    }
}