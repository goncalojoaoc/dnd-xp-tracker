package com.goncalojoaoc.xptracker.presentation.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color

private val DarkColorPalette = darkColors(
    primary = purple500,
    primaryVariant = purple700,
    secondary = purple200,
    onBackground = Color.White,
    onPrimary = Color.White,
    onSurface = Color.White
)

private val LightColorPalette = lightColors(
    primary = purple500,
    primaryVariant = purple700,
    secondary = teal200,
    onBackground = Color.Black,
    onPrimary = Color.Black,
    onSurface = Color.Black

    /* Other default colors to override
background = Color.White,
surface = Color.White,
onPrimary = Color.White,
onSecondary = Color.Black,
onBackground = Color.Black,
onSurface = Color.Black,
*/
)
/*
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun textButtonPositiveColors(
    darkTheme: Boolean = isSystemInDarkTheme()
) = object : ButtonColors {

    override fun backgroundColor(enabled: Boolean): State<Color> = mutableStateOf(Color.Transparent)

    override fun contentColor(enabled: Boolean): State<Color> = mutableStateOf(if (darkTheme) Color.White else Color.Black)
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun textButtonNeutralColors(
    darkTheme: Boolean = isSystemInDarkTheme()
) = object : ButtonColors {
    override fun backgroundColor(enabled: Boolean): State<Color> = mutableStateOf(Color.Transparent)

    override fun contentColor(enabled: Boolean): State<Color> = mutableStateOf(if (darkTheme) Color.White else Color.Black)

}*/

@Composable
fun DnDXPTrackerTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = typography,
        shapes = shapes,
        content = content
    )
}