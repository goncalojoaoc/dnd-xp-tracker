package com.goncalojoaoc.xptracker.presentation.session.list

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.goncalojoaoc.xptracker.data.ISessionsRepository
import com.goncalojoaoc.xptracker.data.ISessionsRepository.Companion.FULL_HISTORY

class SessionListViewModel @ViewModelInject constructor(
    private val repository: ISessionsRepository
) : ViewModel() {

    private val campaignId = MutableLiveData<Long>()

    val sessions = campaignId.switchMap { repository.getCampaignSessions(it, FULL_HISTORY) }

    fun loadSessions(campaign: Long) {
        campaignId.postValue(campaign)
    }
}
