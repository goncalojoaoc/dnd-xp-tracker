package com.goncalojoaoc.xptracker.presentation.campaign.details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.material.Surface
import androidx.compose.runtime.Providers
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.goncalojoaoc.xptracker.R
import com.goncalojoaoc.xptracker.model.campaign.Campaign
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.presentation.main.MainViewModel
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme
import com.goncalojoaoc.xptracker.utils.launch
import dagger.hilt.android.AndroidEntryPoint
import dev.chrisbanes.accompanist.insets.AmbientWindowInsets
import dev.chrisbanes.accompanist.insets.ViewWindowInsetObserver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class CampaignDetailsFragment : Fragment() {

    private val viewModel: CampaignDetailsViewModel by viewModels()
    private val activityViewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

            val windowInsets = ViewWindowInsetObserver(this).start()

            setContent {
                Providers(AmbientWindowInsets provides windowInsets) {
                    DnDXPTrackerTheme {
                        Surface {
                            CampaignDetails(
                                CampaignDetailsView()
                            )
                        }
                    }
                }
            }
        }
    }

    private fun navigateToSession(session: Session) {
        findNavController().navigate(
            CampaignDetailsFragmentDirections.actionCampaignDetailsToSessionFragment(
                session.number,
                session.campaignId
            )
        )
    }

    private fun navigateToSessionReport(session: Session) {
        findNavController().navigate(
            CampaignDetailsFragmentDirections.actionCampaignDetailsToSessionReportFragment(
                session.number,
                session.campaignId
            )
        )
    }

    private fun navigateToCampaignEdit(campaignId: Long) {
        findNavController().navigate(
            CampaignDetailsFragmentDirections.actionCampaignDetailsToCreateCampaignFragment()
                .setCampaignId(campaignId)
        )
    }

    private fun navigateToNewCampaign() {
        findNavController().navigate(
            CampaignDetailsFragmentDirections.actionCampaignDetailsToCreateCampaignFragment()
        )
    }

    private fun navigateToPastSessions(campaignId: Long) {
        findNavController().navigate(
            CampaignDetailsFragmentDirections.actionCampaignDetailsToSessionListFragment(campaignId)
        )
    }

    private inner class CampaignDetailsView : CampaignDetailsContract {

        override val campaign: LiveData<Campaign>
            get() = viewModel.campaign
        override val recentSessions: LiveData<List<Session>>
            get() = viewModel.recentSessions

        override fun onNavIconPressed() = activityViewModel.openDrawer()

        override fun onNewSessionPressed() {
            lifecycleScope.launch(Dispatchers.Default) {

                val session = viewModel.newSession()

                launch(Dispatchers.Main) {
                    if (session != null) {
                        navigateToSession(session)
                    } else {
                        Toast.makeText(
                            requireContext(),
                            R.string.new_session_error,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }

        override fun onOpenSessionPressed(session: Session) {
            navigateToSession(session)
        }

        override fun openSessionReport(session: Session) {
            navigateToSessionReport(session)
        }

        override fun onEditCampaign(campaignId: Long) {
            navigateToCampaignEdit(campaignId)
        }

        override fun onDeleteCampaign() {
            launch(Dispatchers.Default) {
                viewModel.delete()
            }
        }

        override fun onNewCampaign() {
            navigateToNewCampaign()
        }

        override fun onViewPastSessions(campaignId: Long) {
            navigateToPastSessions(campaignId)
        }

    }

}