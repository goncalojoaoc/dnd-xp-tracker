package com.goncalojoaoc.xptracker.presentation.session

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.compose.material.Surface
import androidx.compose.runtime.Providers
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterInSession
import com.goncalojoaoc.xptracker.presentation.model.SessionIdentifier
import com.goncalojoaoc.xptracker.presentation.theme.DnDXPTrackerTheme
import com.goncalojoaoc.xptracker.utils.launch
import dagger.hilt.android.AndroidEntryPoint
import dev.chrisbanes.accompanist.insets.AmbientWindowInsets
import dev.chrisbanes.accompanist.insets.ExperimentalAnimatedInsets
import dev.chrisbanes.accompanist.insets.ViewWindowInsetObserver
import kotlinx.coroutines.Dispatchers

@AndroidEntryPoint
class SessionFragment : Fragment() {

    private val args: SessionFragmentArgs by navArgs()

    private val viewModel: SessionViewModel by viewModels()

    @ExperimentalAnimatedInsets
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

            val windowInsets = ViewWindowInsetObserver(this).start(windowInsetsAnimationsEnabled = true)

            setContent {
                Providers(AmbientWindowInsets provides windowInsets) {
                    DnDXPTrackerTheme {
                        Surface {
                            SessionView(model = SessionView())
                        }
                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.loadSession(
            SessionIdentifier(
                campaignId = args.campaignId,
                sessionNumber = args.sessionNumber
            )
        )
    }

    private fun navigateToSessionReport() {
        findNavController().navigate(
            SessionFragmentDirections.actionSessionFragmentToSessionReportFragment(
                args.sessionNumber,
                args.campaignId
            )
        )
    }

    private inner class SessionView : SessionViewContract {

        override val session: LiveData<Session> = viewModel.session
        override val characters: LiveData<List<CharacterInSession>> = viewModel.charactersInSession

        override fun onNavIconPressed() {
            findNavController().navigateUp()
        }

        override fun awardEncounterXp(value: Int) {
            launch(Dispatchers.Default) {
                viewModel.awardEncounterXp(value)
            }
        }

        override fun awardEpicMoment(characterId: Long) {
            launch(Dispatchers.Default) {
                viewModel.awardEpicMoment(characterId)
            }
        }

        override fun finishSession() {
            launch(Dispatchers.Default) {
                viewModel.endSession()
                launch(Dispatchers.Main) {
                    navigateToSessionReport()
                }
            }
        }

        override fun hideSoftKeyboard() {
            val imm: InputMethodManager? =
                activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager
            var view = activity?.currentFocus
            if (view == null && activity != null) {
                view = View(activity)
            }
            view?.let { imm?.hideSoftInputFromWindow(view.windowToken, 0) }
        }

    }
}