package com.goncalojoaoc.xptracker.presentation.session

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Done
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.goncalojoaoc.xptracker.R
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancement
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancementTable.hasLeveledUp
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancementTable.levelAfterSession
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterInSession
import com.goncalojoaoc.xptracker.model.session.character_advancement.EpicMoment
import com.goncalojoaoc.xptracker.presentation.components.Appbar
import com.goncalojoaoc.xptracker.presentation.components.ConfirmationDialog
import com.goncalojoaoc.xptracker.presentation.components.getString
import com.goncalojoaoc.xptracker.presentation.theme.*
import dev.chrisbanes.accompanist.insets.ProvideWindowInsets
import dev.chrisbanes.accompanist.insets.navigationBarsWithImePadding
import dev.chrisbanes.accompanist.insets.statusBarsHeight
import java.util.*

interface SessionViewContract {
    val session: LiveData<Session>
    val characters: LiveData<List<CharacterInSession>>

    fun onNavIconPressed()
    fun awardEncounterXp(value: Int)
    fun awardEpicMoment(characterId: Long)
    fun finishSession()
    fun hideSoftKeyboard()
}

@Composable
fun SessionView(model: SessionViewContract) {
    val session by model.session.observeAsState()

    Column(modifier = Modifier.fillMaxSize()) {

        Spacer(Modifier.statusBarsHeight())

        Appbar(
            title = {
                Text(text = session?.formattedName().orEmpty())
            },
            navigationIcon = Icons.Default.ArrowBack,
            actions = {
                val (showDialog, setShowDialog) = remember { mutableStateOf(false) }
                ConfirmationDialog(
                    showDialog = showDialog,
                    setShowDialog = setShowDialog,
                    onConfirm = model::finishSession,
                    title = R.string.session_end_confirm_title,
                    message = R.string.session_end_confirm_message,
                    positiveButton = R.string.session_end_confirm_yes,
                    dismissButton = R.string.session_end_confirm_no
                )
                IconButton(onClick = { setShowDialog(true) }) {
                    Icon(Icons.Default.Done, tint = MaterialTheme.colors.onPrimary, contentDescription = null)
                }
            },
            onNavIconPressed = model::onNavIconPressed
        )

        Characters(
            modifier = Modifier.weight(1f),
            characters = model.characters,
            awardEpicMoment = model::awardEpicMoment
        )

        EncounterWidget(
            awardXp = model::awardEncounterXp,
            hideSoftKeyboard = model::hideSoftKeyboard
        )

    }
}

@Composable
private fun Characters(
    modifier: Modifier,
    characters: LiveData<List<CharacterInSession>>,
    awardEpicMoment: (Long) -> Unit
) {
    val chars by characters.observeAsState()

    LazyColumn(modifier = modifier, content = {
        items(chars?.size ?: 0) { i ->
            chars?.let { list ->
                Character(list[i], awardEpicMoment)
            }
        }
    })
}

@Composable
private fun Character(character: CharacterInSession, awardEpicMoment: (Long) -> Unit) {
    Card(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
            .padding(top = 16.dp),
        elevation = cardElevation
    ) {
        ConstraintLayout(
            Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp)) {
            val (star, level, name, epicMoments, button) = createRefs()
            if (character.hasLeveledUp) {
                Image(
                    modifier = Modifier
                        .constrainAs(star) {
                            top.linkTo(parent.top)
                            start.linkTo(level.start)
                            end.linkTo(level.end)
                            bottom.linkTo(level.top)
                        }
                        .rotate(270f),
                    imageVector = vectorResource(R.drawable.ic_double_arrow),
                    colorFilter = ColorFilter.tint(MaterialTheme.colors.secondary),
                    alignment = Alignment.Center,
                    contentDescription = null
                )
            }
            Text(
                modifier = Modifier
                    .constrainAs(level) {
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                        top.linkTo(if (character.hasLeveledUp) star.bottom else parent.top)
                    }
                    .padding(horizontal = 8.dp),
                text = getString(R.string.character_level, character.levelAfterSession),
                style = MaterialTheme.typography.caption,
                maxLines = 1
            )
            Column(
                Modifier
                    .constrainAs(name) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(level.end)
                        end.linkTo(epicMoments.start)
                        width = Dimension.fillToConstraints
                    }
                    .padding(horizontal = 8.dp)) {
                Text(
                    modifier = Modifier.padding(bottom = 4.dp),
                    text = character.name,
                    style = MaterialTheme.typography.body1,
                    fontWeight = FontWeight.Bold,
                    maxLines = 1
                )
                Text(
                    text = getString(R.string.xp_this_session, character.sessionXp),
                    style = MaterialTheme.typography.caption,
                    maxLines = 1
                )
            }
            Column(Modifier.constrainAs(epicMoments) {
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                start.linkTo(name.end)
                end.linkTo(button.start)
            }) {
                Text(
                    modifier = Modifier.padding(bottom = 4.dp),
                    text = getString(R.string.epic_moment_counter_label),
                    style = MaterialTheme.typography.caption,
                    maxLines = 1
                )
                Text(
                    getString(
                        R.string.xp_from_epic_moments,
                        character.advancement.epicMomentsCount,
                        character.advancement.epicMomentsXp
                    ),
                    maxLines = 1
                )
            }

            IconButton(
                modifier = Modifier.constrainAs(button) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    end.linkTo(parent.end)
                }.padding(start = 12.dp),
                onClick = { awardEpicMoment(character.characterId) }
            ) {
                Icon(Icons.Default.Add, tint = MaterialTheme.colors.onSurface, contentDescription = null)
            }
        }

    }
}

@Composable
private fun EncounterWidget(
    awardXp: (Int) -> Unit,
    hideSoftKeyboard: () -> Unit
) {
    Column(
        modifier = Modifier
            .background(
                MaterialTheme.colors.elevatedSurface(
                    elevation = cardElevation
                )
            )
            .navigationBarsWithImePadding()
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .padding(top = 8.dp, bottom = 16.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            var textState by remember { mutableStateOf(TextFieldValue()) }

            fun TextFieldValue.toXpValue(): Int? = text.toIntOrNull()

            fun shouldEnableButton() =
                textState.toXpValue() != null && (textState.toXpValue() ?: 0) > 0

            fun award() {
                awardXp(textState.toXpValue() ?: 0)
                textState = TextFieldValue()
                hideSoftKeyboard()
            }

            OutlinedTextField(
                modifier = Modifier.weight(0.6f),
                value = textState,
                keyboardOptions = KeyboardOptions(
                    autoCorrect = false,
                    keyboardType = KeyboardType.Number,
                    imeAction = ImeAction.Done
                ),
                activeColor = MaterialTheme.colors.onSurface,
                label = { Text(getString(R.string.encounter_xp_title)) },
                onImeActionPerformed = { action, _ ->
                    if (action == ImeAction.Done) {
                        award()
                    }
                },
                onValueChange = { textState = it })

            Spacer(modifier = Modifier.weight(0.1f))

            Button(
                modifier = Modifier
                    .weight(0.3f)
                    .padding(top = 8.dp),
                enabled = shouldEnableButton(),
                onClick = {
                    award()
                }
            ) {
                Text(text = getString(R.string.award_xp))
            }
        }
    }
}

@Composable
private fun Session.formattedName(): String =
    name ?: getString(R.string.session_default_name, number)

@Composable
@Preview
fun Preview() {
    ProvideWindowInsets {
        DnDXPTrackerTheme(darkTheme = true) {
            Surface {
                SessionView(model = object : SessionViewContract {
                    override val session: LiveData<Session> = MutableLiveData(
                        Session(
                            date = Date(),
                            campaignId = -1,
                            number = 1,
                            inProgress = true
                        )
                    )

                    override val characters: LiveData<List<CharacterInSession>> = MutableLiveData(
                        listOf(
                            CharacterInSession(
                                campaignId = -1,
                                name = "Dingleberry",
                                sessionNumber = 1,
                                advancement = CharacterAdvancement(),
                                startingXp = 0,
                                characterId = 1
                            ),
                            CharacterInSession(
                                campaignId = -1,
                                name = "Dungus McFife",
                                sessionNumber = 1,
                                advancement = CharacterAdvancement(
                                    EpicMoment(15)
                                ),
                                startingXp = 0,
                                characterId = 2
                            )
                        )
                    )

                    override fun onNavIconPressed() {}
                    override fun awardEncounterXp(value: Int) {}
                    override fun awardEpicMoment(characterId: Long) {}
                    override fun finishSession() {}
                    override fun hideSoftKeyboard() {}
                })
            }
        }
    }
}
