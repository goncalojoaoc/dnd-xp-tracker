package com.goncalojoaoc.xptracker.presentation.model

data class SessionIdentifier(
    val sessionNumber: Int,
    val campaignId: Long
)
