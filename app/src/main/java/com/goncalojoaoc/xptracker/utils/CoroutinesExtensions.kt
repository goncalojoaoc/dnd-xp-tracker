package com.goncalojoaoc.xptracker.utils

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

fun LifecycleOwner.launch(
    dispatcher: CoroutineDispatcher,
    block: suspend CoroutineScope.() -> Unit
) {
    lifecycleScope.launch(dispatcher) { block() }
}
