package com.goncalojoaoc.xptracker.utils

import android.util.Log
import javax.inject.Inject

class AndroidLogger @Inject constructor() : ILogger {

    override fun d(tag: String, message: String) {
        Log.d(tag, message)
    }

    override fun e(tag: String, message: String, cause: Throwable?) {
        Log.e(tag, message, cause)
    }

}