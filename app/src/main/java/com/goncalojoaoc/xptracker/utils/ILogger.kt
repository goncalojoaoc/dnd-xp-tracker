package com.goncalojoaoc.xptracker.utils

interface ILogger {

    fun d(tag: String, message: String)

    fun e(tag: String, message: String, cause: Throwable? = null)
}
