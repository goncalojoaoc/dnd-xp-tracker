package com.goncalojoaoc.xptracker

import android.app.Application
import com.goncalojoaoc.xptracker.data.IBootstrapper
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class XpTrackerApplication : Application() {

    companion object {
        lateinit var instance: XpTrackerApplication
    }

    @Inject lateinit var bootstrapper: IBootstrapper

    override fun onCreate() {
        instance = this
        super.onCreate()

        bootstrapper.ensureDefaultData()
    }
}