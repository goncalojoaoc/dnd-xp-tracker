package com.goncalojoaoc.xptracker.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterInSession

@Dao
interface CharactersInSessionDao {

    @Query("SELECT * FROM characterinsession WHERE campaignId == :campaignId AND sessionNumber == :sessionNumber")
    fun get(campaignId: Long, sessionNumber: Int): LiveData<List<CharacterInSession>>

    @Insert
    suspend fun insert(character: CharacterInSession)

    @Update
    suspend fun update(vararg character: CharacterInSession)

    @Delete
    suspend fun delete(character: CharacterInSession)

    @Query("DELETE FROM characterinsession WHERE campaignId == :campaignId AND sessionNumber == :sessionNumber")
    suspend fun deleteAllInSession(campaignId: Long, sessionNumber: Int)
}
