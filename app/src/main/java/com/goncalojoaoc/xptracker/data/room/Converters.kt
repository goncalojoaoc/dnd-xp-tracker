package com.goncalojoaoc.xptracker.data.room

import androidx.room.TypeConverter
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancement
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.TypeAdapterFactory
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.util.*
import kotlin.jvm.internal.Reflection
import kotlin.reflect.KClass

class Converters {

    val gson by lazy {
        GsonBuilder().registerTypeAdapterFactory(object : TypeAdapterFactory {
            override fun <T : Any> create(gson: Gson, type: TypeToken<T>): TypeAdapter<T> {
                val kclass = Reflection.getOrCreateKotlinClass(type.rawType)
                return if (kclass.sealedSubclasses.any()) {
                    SealedClassTypeAdapter<T>(kclass, gson)
                } else {
                    gson.getDelegateAdapter(this, type)
                }
            }
        }).create()
    }

    class SealedClassTypeAdapter<T : Any>(private val kclass: KClass<Any>, private val gson: Gson) : TypeAdapter<T>() {

        @Suppress("UNCHECKED_CAST")
        override fun read(jsonReader: JsonReader): T? {
            jsonReader.beginObject() //start reading the object
            val nextName = jsonReader.nextName() //get the name on the object
            val innerClass = kclass.sealedSubclasses.firstOrNull {
                it.simpleName!!.contains(nextName)
            } ?: throw Exception("$nextName is not found to be a data class of the sealed class ${kclass.qualifiedName}")
            val x = gson.fromJson<T>(jsonReader, innerClass.javaObjectType)
            jsonReader.endObject()
            //if there a static object, actually return that back to ensure equality and such!
            return innerClass.objectInstance as T? ?: x
        }

        @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
        override fun write(out: JsonWriter, value: T) {
            val jsonString = gson.toJson(value)
            out.beginObject()
            out.name(value.javaClass.canonicalName.splitToSequence(".").last()).jsonValue(jsonString)
            out.endObject()
        }

    }

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? = value?.let { Date(it) }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? = date?.time

    @TypeConverter
    fun fromCharacterAdvancement(characterAdvancement: CharacterAdvancement): String =
        gson.toJson(characterAdvancement)

    @TypeConverter
    fun toCharacterAdvancement(characterAdvancement: String): CharacterAdvancement {
        return gson.fromJson(characterAdvancement, CharacterAdvancement::class.java)
    }
}