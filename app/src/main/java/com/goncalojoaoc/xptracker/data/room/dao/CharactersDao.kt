package com.goncalojoaoc.xptracker.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.goncalojoaoc.xptracker.model.character.Character

@Dao
interface CharactersDao {

    @Query("SELECT * FROM character WHERE id == :id")
    fun get(id: Long): LiveData<Character>

    @Query("SELECT * FROM character")
    fun getAll(): LiveData<List<Character>>

    @Query("SELECT * FROM character WHERE campaignId == :campaignId")
    fun getFromCampaign(campaignId: Long): LiveData<List<Character>>

    @Insert
    suspend fun insert(character: Character): Long

    @Update
    suspend fun update(characters: List<Character>)

    @Delete
    suspend fun delete(character: Character)

    @Delete
    suspend fun delete(characters: List<Character>)
}