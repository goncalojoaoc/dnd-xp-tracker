package com.goncalojoaoc.xptracker.data.room.repository

import androidx.lifecycle.LiveData
import com.goncalojoaoc.xptracker.data.ICampaignsRepository
import com.goncalojoaoc.xptracker.data.room.dao.CampaignsDao
import com.goncalojoaoc.xptracker.data.room.dao.CharactersDao
import com.goncalojoaoc.xptracker.data.room.dao.SessionsDao
import com.goncalojoaoc.xptracker.model.campaign.Campaign
import com.goncalojoaoc.xptracker.model.campaign.CampaignMetadata
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class RoomCampaignsRepository @Inject constructor(
    private val campaignsDao: CampaignsDao,
    private val charactersDao: CharactersDao,
    private val sessionsDao: SessionsDao
) : ICampaignsRepository {

    override fun getCampaignMetadata(): LiveData<List<CampaignMetadata>> =
        campaignsDao.getMetadata()

    override fun getCampaignMetadata(campaignId: Long): LiveData<CampaignMetadata> =
        campaignsDao.getMetadata(campaignId)

    override fun getCampaignById(id: Long): LiveData<Campaign> = campaignsDao.getById(id)

    override fun getActiveCampaign(): LiveData<CampaignMetadata?> = campaignsDao.getActive()

    override fun setActiveCampaign(campaignId: Long) {
        // Using GlobalScope here because we want this to be a fire-and-forget thing
        GlobalScope.launch(Dispatchers.Default) {
            campaignsDao.setActive(campaignId)
        }
    }

    override suspend fun createCampaign(campaignMetadata: CampaignMetadata): Long =
        campaignsDao.insert(campaignMetadata)

    override suspend fun updateMetadata(campaignMetadata: CampaignMetadata) {
        campaignsDao.update(campaignMetadata)
    }

    override suspend fun delete(campaign: Campaign) {
        campaignsDao.delete(campaign.metadata)
        charactersDao.delete(campaign.characters)
        sessionsDao.deleteAllCampaignSessions(campaign.metadata.id)
    }

}