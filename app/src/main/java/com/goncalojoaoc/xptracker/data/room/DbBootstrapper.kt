package com.goncalojoaoc.xptracker.data.room

import androidx.annotation.IntRange
import com.goncalojoaoc.xptracker.AppModule.Companion.IS_DEBUG
import com.goncalojoaoc.xptracker.data.IBootstrapper
import com.goncalojoaoc.xptracker.data.room.dao.CampaignsDao
import com.goncalojoaoc.xptracker.data.room.dao.CharactersDao
import com.goncalojoaoc.xptracker.data.room.dao.CharactersInSessionDao
import com.goncalojoaoc.xptracker.data.room.dao.SessionsDao
import com.goncalojoaoc.xptracker.model.campaign.CampaignMetadata
import com.goncalojoaoc.xptracker.model.character.Character
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancement
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterInSession
import com.goncalojoaoc.xptracker.utils.ILogger
import com.goncalojoaoc.xptracker.utils.await
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import javax.inject.Named
import kotlin.random.Random

@Suppress("unused")
class DbBootstrapper @Inject constructor(
    @Named(IS_DEBUG) private val isDebug: Boolean,
    private val charactersDao: CharactersDao,
    private val campaignsDao: CampaignsDao,
    private val sessionsDao: SessionsDao,
    private val charactersInSessionDao: CharactersInSessionDao,
    private val log: ILogger
) : IBootstrapper {

    companion object {
        private val TAG = DbBootstrapper::class.simpleName ?: ""

        private const val CAMPAIGNS_TO_CREATE = 3

        private val characterNames = listOf(
            "Lord Mc Edgelord II",
            "War-locko",
            "Bardbarian",
            "Lady Bonks-a-lot",
            "Megumin",
            "Asexual Bard",
            "Tabaxi Monk",
            "Jack Sparrot",
            "Horny the Satyr",
            "The Ruby of the Sea",
            "Jeff"
        )

        private val campaignNames = listOf(
            "Bardemic",
            "Necromancers R Us",
            "The Idiot Emporium",
            "Caves and Lizards",
            "Two Humans and a Half(ling)"
        )
    }

    override fun ensureDefaultData() {
        if (!isDebug) return

        // Using GlobalScope here because we don't care when this is done
        GlobalScope.launch(Dispatchers.Default) {
            val campaigns = campaignsDao.getAll().await()
            if (campaigns.isEmpty()) {
                log.d(TAG, "Bootstrapping campaigns")
                try {
                    bootstrapCampaigns(CAMPAIGNS_TO_CREATE)
                } catch (ex: Exception) {
                    log.e(TAG, "Failed to bootstrap", ex)
                }
            }
        }
    }

    private suspend fun bootstrapCampaigns(@IntRange(from = 0, to = 3) amount: Int) {
        campaignNames.shuffled().take(amount).forEachIndexed { index, name ->
            val campaignId = campaignsDao.insert(
                CampaignMetadata(name = name, isActive = index == 0)
            )
            bootstrapCharacters(Random.nextInt(2, 5), campaignId)
            //if (index == 0) {
                //bootstrapSessions(campaignId)
            //}
        }
    }

    private suspend fun bootstrapCharacters(
        @IntRange(from = 0, to = 5) amount: Int,
        campaignId: Long
    ) {
        characterNames.shuffled().take(amount).forEach {
            charactersDao.insert(
                Character(name = it, campaignId = campaignId)
            )
        }
    }

    private suspend fun bootstrapSessions(campaignId: Long) {
        (0 until Random.nextInt(4, 10)).forEach { i ->
            sessionsDao.insert(
                Session(
                    date = Date(),
                    campaignId = campaignId,
                    number = i,
                    inProgress = false
                )
            )
            campaignsDao.getById(campaignId).await().characters.forEach {
                charactersInSessionDao.insert(
                    CharacterInSession(
                        characterId = it.id,
                        campaignId = campaignId,
                        sessionNumber = i,
                        startingXp = it.xp,
                        name = it.name,
                        advancement = CharacterAdvancement()
                    )
                )
            }
        }
    }
}
