package com.goncalojoaoc.xptracker.data

import androidx.lifecycle.LiveData
import com.goncalojoaoc.xptracker.model.campaign.Campaign
import com.goncalojoaoc.xptracker.model.campaign.CampaignMetadata

interface ICampaignsRepository {

    companion object {
        const val NO_ACTIVE_CAMPAIGN: Long = -1L
    }

    fun getCampaignMetadata(): LiveData<List<CampaignMetadata>>

    fun getCampaignMetadata(campaignId: Long): LiveData<CampaignMetadata>

    fun getCampaignById(id: Long): LiveData<Campaign>

    fun getActiveCampaign(): LiveData<CampaignMetadata?>

    fun setActiveCampaign(campaignId: Long)

    suspend fun createCampaign(campaignMetadata: CampaignMetadata): Long

    suspend fun updateMetadata(campaignMetadata: CampaignMetadata)

    suspend fun delete(campaign: Campaign)
}