package com.goncalojoaoc.xptracker.data.room.repository

import androidx.lifecycle.LiveData
import com.goncalojoaoc.xptracker.data.ICharactersRepository
import com.goncalojoaoc.xptracker.data.room.dao.CharactersDao
import com.goncalojoaoc.xptracker.model.character.Character
import javax.inject.Inject

class RoomCharactersRepository @Inject constructor(
    private val charactersDao: CharactersDao
) : ICharactersRepository {

    override suspend fun create(character: Character): Long = charactersDao.insert(character)

    override fun get(id: Long): LiveData<Character> = charactersDao.get(id)

    override suspend fun update(characters: List<Character>) = charactersDao.update(characters)

    override suspend fun delete(character: Character) = charactersDao.delete(character)

    override fun getCharactersInCampaign(campaignId: Long): LiveData<List<Character>> =
        charactersDao.getFromCampaign(campaignId)
}
