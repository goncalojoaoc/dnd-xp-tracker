package com.goncalojoaoc.xptracker.data

import androidx.lifecycle.LiveData
import com.goncalojoaoc.xptracker.model.character.Character

interface ICharactersRepository {

    suspend fun create(character: Character): Long

    fun get(id: Long): LiveData<Character>

    suspend fun update(characters: List<Character>)

    suspend fun delete(character: Character)

    fun getCharactersInCampaign(campaignId: Long): LiveData<List<Character>>
}
