package com.goncalojoaoc.xptracker.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.goncalojoaoc.xptracker.model.campaign.Campaign
import com.goncalojoaoc.xptracker.model.campaign.CampaignMetadata
import com.goncalojoaoc.xptracker.utils.await

@Dao
abstract class CampaignsDao {

    @Transaction
    @Query("SELECT * FROM campaigns")
    abstract fun getAll(): LiveData<List<Campaign>>

    @Query("SELECT * FROM campaigns WHERE id == :campaignId")
    abstract fun getMetadata(campaignId: Long): LiveData<CampaignMetadata>

    @Query("SELECT * FROM campaigns")
    abstract fun getMetadata(): LiveData<List<CampaignMetadata>>

    @Transaction
    @Query("SELECT * FROM campaigns WHERE id == :id")
    abstract fun getById(id: Long): LiveData<Campaign>

    @Query("SELECT * FROM campaigns WHERE isActive")
    abstract fun getActive(): LiveData<CampaignMetadata?>

    @Transaction
    open suspend fun setActive(campaignId: Long) {
        getMetadata().await().forEach { campaign ->
            when {
                !campaign.isActive && campaign.id == campaignId -> {
                    update(campaign.copy(isActive = true))
                }
                campaign.isActive && campaign.id != campaignId -> {
                    update(campaign.copy(isActive = false))
                }
            }
        }
    }

    @Insert
    abstract suspend fun insert(campaign: CampaignMetadata): Long

    @Update
    abstract suspend fun update(campaign: CampaignMetadata)

    @Delete
    abstract suspend fun delete(campaign: CampaignMetadata)
}