package com.goncalojoaoc.xptracker.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.goncalojoaoc.xptracker.data.room.dao.CampaignsDao
import com.goncalojoaoc.xptracker.data.room.dao.CharactersDao
import com.goncalojoaoc.xptracker.data.room.dao.CharactersInSessionDao
import com.goncalojoaoc.xptracker.data.room.dao.SessionsDao
import com.goncalojoaoc.xptracker.model.campaign.CampaignMetadata
import com.goncalojoaoc.xptracker.model.character.Character
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterInSession

@TypeConverters(Converters::class)
@Database(
    entities = [Character::class, CampaignMetadata::class, Session::class, CharacterInSession::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun campaignDao(): CampaignsDao
    abstract fun charactersDao(): CharactersDao
    abstract fun sessionsDao(): SessionsDao
    abstract fun charactersInSessionDao(): CharactersInSessionDao
}