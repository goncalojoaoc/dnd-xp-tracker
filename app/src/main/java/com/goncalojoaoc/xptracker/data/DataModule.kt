package com.goncalojoaoc.xptracker.data

import com.goncalojoaoc.xptracker.data.room.RoomModule
import com.goncalojoaoc.xptracker.data.room.repository.RoomCampaignsRepository
import com.goncalojoaoc.xptracker.data.room.repository.RoomCharactersRepository
import com.goncalojoaoc.xptracker.data.room.repository.RoomSessionsRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module(includes = [RoomModule::class])
@InstallIn(ApplicationComponent::class)
abstract class DataModule {

    @Binds
    abstract fun bindCampaignRepository(impl: RoomCampaignsRepository): ICampaignsRepository

    @Binds
    abstract fun bindSessionsRepository(impl: RoomSessionsRepository): ISessionsRepository

    @Binds
    abstract fun bindCharactersRepository(impl: RoomCharactersRepository): ICharactersRepository
}
