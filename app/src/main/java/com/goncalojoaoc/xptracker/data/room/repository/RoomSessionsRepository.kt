package com.goncalojoaoc.xptracker.data.room.repository

import androidx.lifecycle.LiveData
import com.goncalojoaoc.xptracker.data.ISessionsRepository
import com.goncalojoaoc.xptracker.data.room.dao.CharactersDao
import com.goncalojoaoc.xptracker.data.room.dao.CharactersInSessionDao
import com.goncalojoaoc.xptracker.data.room.dao.SessionsDao
import com.goncalojoaoc.xptracker.model.mapper.CharacterInSessionMapper
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterInSession
import com.goncalojoaoc.xptracker.utils.await
import java.util.*
import javax.inject.Inject

class RoomSessionsRepository @Inject constructor(
    private val charactersDao: CharactersDao,
    private val sessionsDao: SessionsDao,
    private val charactersInSessionDao: CharactersInSessionDao
) : ISessionsRepository {

    override suspend fun createSessionInCampaign(campaignId: Long, inProgress: Boolean): Session {
        val number = sessionsDao.mostRecentSessionNumber(campaignId) ?: 0
        val characters = charactersDao.getFromCampaign(campaignId).await()

        val session = Session(
            campaignId = campaignId,
            number = number + 1,
            inProgress = inProgress,
            date = Date()
        )
        sessionsDao.insert(session)

        // Ensure the session has characters prepared to receive XP
        characters.forEach {
            charactersInSessionDao.insert(CharacterInSessionMapper.map(it, session))
        }

        return session
    }

    override fun getCampaignSessions(campaignId: Long, maximumHistory: Int): LiveData<List<Session>> =
        if (maximumHistory > 0) {
            sessionsDao.getRecentCampaignSessions(campaignId = campaignId, maximum = maximumHistory)
        } else {
            sessionsDao.getCampaignSessions(campaignId)
        }

    override fun getSession(campaignId: Long, sessionNumber: Int): LiveData<Session> =
        sessionsDao.getSession(campaignId = campaignId, number = sessionNumber)

    override suspend fun updateSession(session: Session) {
        sessionsDao.update(session)
    }

    override fun getCharactersInSession(
        campaignId: Long,
        sessionNumber: Int
    ): LiveData<List<CharacterInSession>> = charactersInSessionDao.get(campaignId, sessionNumber)

    override suspend fun updateCharacterInSession(character: CharacterInSession) {
        charactersInSessionDao.update(character)
    }

    override suspend fun updateAllCharactersInSession(characters: List<CharacterInSession>) {
        charactersInSessionDao.update(*characters.toTypedArray())
    }

}
