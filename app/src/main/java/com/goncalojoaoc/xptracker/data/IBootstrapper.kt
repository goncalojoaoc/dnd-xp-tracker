package com.goncalojoaoc.xptracker.data

interface IBootstrapper {

    fun ensureDefaultData()
}