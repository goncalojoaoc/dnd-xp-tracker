package com.goncalojoaoc.xptracker.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.goncalojoaoc.xptracker.model.session.Session

@Dao
interface SessionsDao {

    @Query("SELECT * FROM session WHERE campaignId == :campaignId ORDER BY date DESC LIMIT :maximum")
    fun getRecentCampaignSessions(campaignId: Long, maximum: Int): LiveData<List<Session>>

    @Query("SELECT * FROM session WHERE campaignId == :campaignId ORDER BY date")
    fun getCampaignSessions(campaignId: Long): LiveData<List<Session>>

    @Query("SELECT * FROM session WHERE campaignId == :campaignId AND number == :number")
    fun getSession(campaignId: Long, number: Int): LiveData<Session>

    @Query("SELECT number FROM session WHERE campaignId == :campaignId ORDER BY date DESC LIMIT 1")
    suspend fun mostRecentSessionNumber(campaignId: Long): Int?

    @Insert
    suspend fun insert(session: Session)

    @Update
    suspend fun update(session: Session)

    @Query("DELETE FROM session WHERE campaignId == :campaignId")
    suspend fun deleteAllCampaignSessions(campaignId: Long)

}
