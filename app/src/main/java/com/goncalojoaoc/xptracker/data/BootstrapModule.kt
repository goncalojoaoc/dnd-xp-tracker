package com.goncalojoaoc.xptracker.data

import com.goncalojoaoc.xptracker.data.room.DbBootstrapper
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module
@InstallIn(ApplicationComponent::class)
abstract class BootstrapModule {

    @Binds
    abstract fun bindBootstraper(impl: DbBootstrapper): IBootstrapper
}