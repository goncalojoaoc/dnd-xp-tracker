package com.goncalojoaoc.xptracker.data.room

import android.content.Context
import androidx.room.Room
import com.goncalojoaoc.xptracker.data.room.dao.CampaignsDao
import com.goncalojoaoc.xptracker.data.room.dao.CharactersDao
import com.goncalojoaoc.xptracker.data.room.dao.CharactersInSessionDao
import com.goncalojoaoc.xptracker.data.room.dao.SessionsDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class RoomModule {

    companion object {
        private const val DATABASE_NAME = "XpTrackerDatabase"
    }

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase =
        Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            DATABASE_NAME
        ).fallbackToDestructiveMigration().build()

    @Provides
    fun provideCampaignDao(appDatabase: AppDatabase): CampaignsDao = appDatabase.campaignDao()

    @Provides
    fun provideCharactersDao(appDatabase: AppDatabase): CharactersDao = appDatabase.charactersDao()

    @Provides
    fun provideSessionsDao(appDatabase: AppDatabase): SessionsDao = appDatabase.sessionsDao()

    @Provides
    fun provideChractersInSessionDao(appDatabase: AppDatabase): CharactersInSessionDao =
        appDatabase.charactersInSessionDao()

}