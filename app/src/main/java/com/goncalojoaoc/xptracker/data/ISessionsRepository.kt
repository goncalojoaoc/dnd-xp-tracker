package com.goncalojoaoc.xptracker.data

import androidx.lifecycle.LiveData
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterInSession
import com.goncalojoaoc.xptracker.model.session.character_advancement.XpBump

interface ISessionsRepository {

    companion object {
        const val FULL_HISTORY = -1
    }

    suspend fun createSessionInCampaign(campaignId: Long, inProgress: Boolean = true): Session

    fun getCampaignSessions(campaignId: Long, maximumHistory: Int): LiveData<List<Session>>

    fun getSession(campaignId: Long, sessionNumber: Int): LiveData<Session>

    suspend fun updateSession(session: Session)

    fun getCharactersInSession(
        campaignId: Long,
        sessionNumber: Int
    ): LiveData<List<CharacterInSession>>

    suspend fun updateCharacterInSession(character: CharacterInSession)

    suspend fun updateAllCharactersInSession(characters: List<CharacterInSession>)
}
