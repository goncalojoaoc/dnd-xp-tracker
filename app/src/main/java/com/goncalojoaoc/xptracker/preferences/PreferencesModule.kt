package com.goncalojoaoc.xptracker.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext

@Module(includes = [PreferencesModule.Bindings::class])
@InstallIn(ApplicationComponent::class)
object PreferencesModule {

    @Provides
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    @Module
    @InstallIn(ApplicationComponent::class)
    interface Bindings {

        @Binds
        abstract fun bindSharedPreferences(impl: AndroidSharedPreferences): ISharedPreferences
    }
}