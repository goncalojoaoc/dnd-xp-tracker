package com.goncalojoaoc.xptracker.preferences

import android.content.SharedPreferences
import javax.inject.Inject

class AndroidSharedPreferences @Inject constructor(
    private val sharedPreferences: SharedPreferences
) : ISharedPreferences, SharedPreferences.OnSharedPreferenceChangeListener {

    private val listeners = mutableMapOf<String, MutableList<() -> Unit>>()

    init {
        sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun putString(key: String, value: String) {
        with(sharedPreferences.edit()) {
            putString(key, value)
            apply()
        }
    }

    override fun getString(key: String, default: String?): String? =
        sharedPreferences.getString(key, default)

    override fun putInt(key: String, value: Int) {
        with(sharedPreferences.edit()) {
            putInt(key, value)
            apply()
        }
    }

    override fun getInt(key: String, default: Int): Int =
        sharedPreferences.getInt(key, default)

    override fun putLong(key: String, value: Long) {
        with(sharedPreferences.edit()) {
            putLong(key, value)
            apply()
        }
    }

    override fun getLong(key: String, default: Long): Long =
        sharedPreferences.getLong(key, default)

    override fun listenForChanges(key: String, onChange: () -> Unit) {
        listeners[key]?.add(onChange) ?: listeners.put(key, mutableListOf(onChange))
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        listeners[key]?.forEach { callback -> callback() }
    }

}