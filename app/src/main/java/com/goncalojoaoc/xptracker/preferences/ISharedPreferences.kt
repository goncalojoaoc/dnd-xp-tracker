package com.goncalojoaoc.xptracker.preferences

interface ISharedPreferences {

    companion object {
        //const val SELECTED_CAMPAIGN_ID = "SelectedCampaignId"
    }

    fun putString(key: String, value: String)

    fun getString(key: String, default: String? = null): String?

    fun putInt(key: String, value: Int)

    fun getInt(key: String, default: Int = -1): Int

    fun putLong(key: String, value: Long)

    fun getLong(key: String, default: Long = -1): Long

    fun listenForChanges(key: String, onChange: () -> Unit)

}