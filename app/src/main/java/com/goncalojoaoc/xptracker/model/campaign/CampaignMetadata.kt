package com.goncalojoaoc.xptracker.model.campaign

import androidx.room.Entity
import androidx.room.PrimaryKey

const val DEFAULT_EPIC_MOMENT_XP_MODIFIER = 15

@Entity(tableName = "campaigns")
data class CampaignMetadata(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val name: String,
    val isActive: Boolean = false,
    val epicMomentsEnabled: Boolean = true,
    val epicMomentXpModifier: Int = DEFAULT_EPIC_MOMENT_XP_MODIFIER
)
