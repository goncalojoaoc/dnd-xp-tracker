package com.goncalojoaoc.xptracker.model.mapper

import com.goncalojoaoc.xptracker.model.character.Character
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancement
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterInSession
import com.goncalojoaoc.xptracker.model.session.Session

object CharacterInSessionMapper {

    fun map(character: Character, session: Session) =
        CharacterInSession(
            characterId = character.id,
            campaignId = session.campaignId,
            sessionNumber = session.number,
            name = character.name,
            startingXp = character.xp,
            advancement = CharacterAdvancement()
        )
}
