package com.goncalojoaoc.xptracker.model.session.report

import android.content.Context
import com.goncalojoaoc.xptracker.R
import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancementTable.hasLeveledUp
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancementTable.levelAfterSession
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterInSession
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class SimplePlayerSessionReportMapper @Inject constructor(
    @ApplicationContext private val context: Context
) : IPlayerSessionReportMapper {

    override fun mapIndividual(session: Session, character: CharacterInSession): String {
        return """
            Congrats, ${character.name} won ${character.sessionXp} this session! They are now level ${character.levelAfterSession}
        """.trimIndent()
    }

    override fun mapAll(session: Session, characters: List<CharacterInSession>): String {
        val sb = StringBuilder("XP awards for ${session.formattedName()}:\n\n")
        characters.forEach { character ->
            sb.append(character.name).append(": ").append(character.sessionXp).append(" XP.")
            if (character.hasLeveledUp) {
                sb.append(" You are now level ").append(character.levelAfterSession).append("!")
            }
            sb.append("\n")
        }
        return sb.toString()
    }

    private fun Session.formattedName(): String =
        name ?: context.getString(R.string.session_default_name, number)
}
