package com.goncalojoaoc.xptracker.model.session.character_advancement

class CharacterAdvancement(data: Collection<XpBump> = emptyList()) : ArrayList<XpBump>(data) {

    constructor(vararg xpBump: XpBump) : this(xpBump.toList())

    val epicMoments: List<EpicMoment>
        get() = filterIsInstance<EpicMoment>()

    val epicMomentsCount: Int
        get() = count { it is EpicMoment }

    val epicMomentsXp: Int
        get() = epicMoments.sumBy { it.awardedXp }

    val encounters: List<Encounter>
        get() = filterIsInstance<Encounter>()

    val encountersCount: Int
        get() = count { it is Encounter }

    val encountersXp: Int
        get() = encounters.sumBy { it.awardedXp }

    val others: List<Other>
        get() = filterIsInstance<Other>()

    val othersCount: Int
        get() = count { it is Other }

    val othersXp: Int
        get() = others.sumBy { it.awardedXp }
}
