package com.goncalojoaoc.xptracker.model.session.character_advancement

sealed class XpBump(
    val awardedXp: Int
)

class Encounter(awardedXp: Int) : XpBump(awardedXp)

class EpicMoment(awardedXp: Int): XpBump(awardedXp)

class Other(
    awardedXp: Int,
    val description: String
): XpBump(awardedXp)
