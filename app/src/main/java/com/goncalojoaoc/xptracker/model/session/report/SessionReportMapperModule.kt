package com.goncalojoaoc.xptracker.model.session.report

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module
@InstallIn(ApplicationComponent::class)
abstract class SessionReportMapperModule {

    @Binds
    abstract fun bindPlayerSessionReportMapper(impl: SimplePlayerSessionReportMapper): IPlayerSessionReportMapper
}
