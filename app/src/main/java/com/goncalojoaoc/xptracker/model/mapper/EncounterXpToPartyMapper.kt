package com.goncalojoaoc.xptracker.model.mapper

object EncounterXpToPartyMapper {

    fun xpPerCharacter(encounterXp: Int, partySize: Int): Int = encounterXp / partySize
}
