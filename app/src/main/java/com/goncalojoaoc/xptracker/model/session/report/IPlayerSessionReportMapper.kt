package com.goncalojoaoc.xptracker.model.session.report

import com.goncalojoaoc.xptracker.model.session.Session
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterInSession

interface IPlayerSessionReportMapper {

    /**
     * Maps a session's report for a given character into a human-readable String, to be shared
     * with the character's player.
     */
    fun mapIndividual(session: Session, character: CharacterInSession): String

    /**
     * Maps a session's report for all character into a human-readable String, to be shared
     * with players.
     */
    fun mapAll(session: Session, characters: List<CharacterInSession>): String
}
