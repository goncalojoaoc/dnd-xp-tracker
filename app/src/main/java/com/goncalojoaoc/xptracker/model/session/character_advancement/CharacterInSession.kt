package com.goncalojoaoc.xptracker.model.session.character_advancement

import androidx.room.Entity

@Entity(primaryKeys = ["characterId", "campaignId", "sessionNumber"])
data class CharacterInSession(
    val characterId: Long,
    val campaignId: Long,
    val sessionNumber: Int,
    val name: String,
    val startingXp: Int,
    val advancement: CharacterAdvancement
) {

    val sessionXp: Int
        get() = advancement.sumBy { it.awardedXp }

    val xpAfterSession: Int
        get() = startingXp + sessionXp

}
