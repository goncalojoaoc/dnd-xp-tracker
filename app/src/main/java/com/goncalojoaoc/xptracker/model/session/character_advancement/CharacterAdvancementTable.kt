package com.goncalojoaoc.xptracker.model.session.character_advancement

import com.goncalojoaoc.xptracker.model.character.Character

object CharacterAdvancementTable {

    internal val startingLevel: Int = 1

    internal val endLevel: Int = 20

    internal val xpLevelThresholds: Map<Int, Int> = mapOf(
        1 to 0,
        2 to 300,
        3 to 900,
        4 to 2_700,
        5 to 6_500,
        6 to 14_000,
        7 to 23_000,
        8 to 34_000,
        9 to 48_000,
        10 to 64_000,
        11 to 85_000,
        12 to 100_000,
        13 to 120_000,
        14 to 140_000,
        15 to 165_000,
        16 to 195_000,
        17 to 225_000,
        18 to 265_000,
        19 to 305_000,
        20 to 355_000
    )

    fun levelAtXp(xp: Int): Int {
        val index = xpLevelThresholds.values.indexOfLast { it <= xp }
        return xpLevelThresholds.keys.toList()[index]
    }

    fun calculateEpicMomentXp(modifier: Int, characterXp: Int) =
        EpicMoment(modifier * levelAtXp(characterXp))

    fun advanceCharacters(
        campaignCharacters: List<Character>,
        sessionCharacters: List<CharacterInSession>
    ) = campaignCharacters.map { char ->
        advanceCharacter(char, sessionCharacters.first { it.characterId == char.id })
    }

    private fun advanceCharacter(character: Character, characterInSession: CharacterInSession): Character =
        character.copy(
            xp = characterInSession.xpAfterSession
        )

    val Character.level: Int
        get() = levelAtXp(xp)

    val CharacterInSession.levelBeforeSession: Int
        get() = levelAtXp(startingXp)

    val CharacterInSession.levelAfterSession: Int
        get() = levelAtXp(xpAfterSession)

    val CharacterInSession.hasLeveledUp: Boolean
        get() = levelBeforeSession < levelAfterSession

}
