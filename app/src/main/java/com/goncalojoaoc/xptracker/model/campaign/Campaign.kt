package com.goncalojoaoc.xptracker.model.campaign

import androidx.room.Embedded
import androidx.room.Relation
import com.goncalojoaoc.xptracker.model.character.Character

data class Campaign(
    @Embedded val metadata: CampaignMetadata,
    @Relation(
        parentColumn = "id",
        entityColumn = "campaignId"
    )
    val characters: List<Character>
)
