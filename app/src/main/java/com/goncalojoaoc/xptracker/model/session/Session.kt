package com.goncalojoaoc.xptracker.model.session

import androidx.room.Entity
import com.goncalojoaoc.xptracker.model.session.character_advancement.CharacterAdvancement
import java.util.*

@Entity(primaryKeys = ["number", "campaignId"])
data class Session(
    val date: Date,
    val number: Int,
    val campaignId: Long,
    val inProgress: Boolean,
    val name: String? = null
)
