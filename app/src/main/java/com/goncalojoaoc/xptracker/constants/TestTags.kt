package com.goncalojoaoc.xptracker.constants

/**
 * Navigation drawer toggle icon
 */
const val NAVIGATION_ICON = "navigation_icon"
const val ADD_CAMPAIGN = "add_campaign"
const val ADD_CHARACTER = "add_character"
const val CHARACTER_NAME = "character_name"
const val CURRENT_XP = "current_xp"
const val DELETE_CHARACTER = "delete_character"
const val EDIT_CHARACTER = "edit_character"
const val DROPDOWN_MENU = "dropdown_menu"
