#!/bin/bash

cd ..
pwd
base64 -d <<< "$KEYSTORE_BASE64" > app/build/outputs/bundle/release/release.jks
ls app/build/outputs/bundle/release/
jarsigner app/build/outputs/bundle/release/app-release.aab "$KEYSTORE_ALIAS" -keystore app/build/outputs/bundle/release/release.jks -storepass "$KEYSTORE_PASSWORD" -keypass "$KEYSTORE_KEY_PASSWORD"

base64 -d <<< "$PLAY_STORE_JSON_BASE64" > fastlane/google.json