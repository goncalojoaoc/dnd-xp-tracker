package com.goncalojoaoc.xptracker.buildsrc

object Versions {

    // Build Tooling
    const val ktlint = "0.39.0"
    const val androidGradlePlugin = "7.0.0-alpha04"
    const val jdkDesugar = "1.0.9"

    // Kotlin
    const val kotlin = "1.4.21-2"
    const val coroutines = "1.4.2"

    // Material Design components
    const val material = "1.2.1"

    // AndroidX
    const val appcompat = "1.2.0"
    const val coreKtx = "1.5.0-beta01"
    const val compose = "1.0.0-alpha11"
    const val navigation = "2.3.2"
    const val androidxText = "1.3.0"
    const val androidxJunitExt = "1.1.2"
    const val lifecycle = "2.2.0"
    const val room = "2.2.6"
    const val preferences = "1.1.1"

    // Hilt
    const val hilt = "2.31.2-alpha"
    const val hiltLifecycle = "1.0.0-alpha02"

    // Other
    const val accompanist = "0.4.2"

    // Testing
    const val junit = "4.13.1"
    const val espresso = "3.3.0"
    const val testRunner = "1.1.0"

}